import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AngularFaviconService } from 'angular-favicon';
import { InterceptorService } from './services/interceptor.service';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inova-portal-cliente';

  get loading() {
    return this.login.loading;
  }

  constructor(private login: LoginService, private favIcon: AngularFaviconService, private t: Title) {
    const registry = JSON.parse(localStorage.getItem('registry'));
    if (registry) {
      this.login.setRegistry(registry);
      this.t.setTitle(this.login.getRegistry.nome);
      this.favIcon.setFavicon(this.login.getRegistry.urlLogo);
    }
  }
}
