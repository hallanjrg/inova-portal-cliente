import { HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, finalize, tap } from 'rxjs/operators';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  private requests: Array<number> = [];


  constructor(private login: LoginService) { }

  removeRequest() {
    this.requests.splice(0, 1)
    if (this.requests.length == 0) {
      setTimeout(() => {
        this.login.loading = false;
      }, 500);
    }
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    this.requests.push(1);
    const token = localStorage.getItem('tkn_prt');
    if (req.url.includes('viacep')) {
      this.login.loading = true;

      return next.handle(req).pipe(
        catchError(err => {
          this.removeRequest();
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    } else if (req.url.includes('secure/correios/calcularfrete')) {
      this.login.loading = true;

      let modifiedReq = req.clone({ headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('sys_tkn')}`) })
      return next.handle(modifiedReq).pipe(
        catchError(err => {
          this.removeRequest();
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    } else if (req.url.includes('secure/cartorio/get')) {
      this.login.loading = true;
      localStorage.removeItem('tkn_prt');
      let modifiedReq = req.clone({ headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('sys_tkn')}`) })
      return next.handle(modifiedReq).pipe(
        catchError(err => {
          this.removeRequest();
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    } /* else if (req.url.includes('notificacoes')) {
      return next.handle(req).pipe(
        catchError(err => {
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    } */ else if (token) {
      this.login.loading = true;
      let modifiedReq = req.clone({ headers: new HttpHeaders().set('Authorization', `Bearer ${token}`) })
      return next.handle(modifiedReq).pipe(
        catchError(err => {
          this.removeRequest();
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    } else {
      this.login.loading = true;
      return next.handle(req).pipe(
        catchError(err => {
          this.removeRequest();
          throw err;
        })).pipe(finalize(() => { this.removeRequest() }));
    }
  }
}
