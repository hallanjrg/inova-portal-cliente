import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import jwt_decode from "jwt-decode";
import { User } from '../models/user';
import { Registry } from '../models/registry';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public loading: boolean = false;
  public registry: Registry = null;
  private user: User = null;
  loggedUser: any;

  constructor(private http: HttpClient) {
    localStorage.getItem('user') ? this.user = JSON.parse(localStorage.getItem('user')) : null;
  }

  get getToken() {
    const token = localStorage.getItem('tkn_prt');
    if (!token) {
      return false;
    } else return token;
  }

  get getUser() {
    return this.user;
  }

  get getRegistry() {
    return this.registry;
  }

  updateUserName(name: string) {
    this.user.sub = name;
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  setUser(token: string) {
    this.user = jwt_decode(token);
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  setRegistry(registry: any) {
    this.registry = registry;
    localStorage.setItem('registry', JSON.stringify(registry));
  }

  resetPassword(param: string, body: any, token: any) {
    return this.http.put(`${environment.url}${param}`, body, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
    })
  }

}
