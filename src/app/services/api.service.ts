import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  /* mocks */

  public mockUrl = 'http://localhost:3001/'

  /* mocks */

  constructor(private http: HttpClient) { }

  get(param: string) {
    return this.http.get(`${environment.url}${param}`)
  }

  put(param: string, body: any) {
    return this.http.put(`${environment.url}${param}`, body)
  }

  post(param: string, body: any, token?: string) {
    return this.http.post(`${environment.url}${param}`, body, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${token}`)
    })
  }

  delete(param: string) {
    return this.http.delete(`${environment.url}${param}`)
  }

  sysToken() {
    return this.http.get(`${environment.url}sys/token`)
  }


  /* mocks */

  getMock(params: string) {
    return this.http.get(`${this.mockUrl}${params}`);
  }

  postMock(params: string, body: any) {
    return this.http.post(`${this.mockUrl}${params}`, body);
  }

  putMock(params: string, body: any) {
    return this.http.put(`${this.mockUrl}${params}`, body);
  }

  deleteMock(params: string) {
    return this.http.delete(`${this.mockUrl}${params}`)
  }

  /* mocks */

  /* cep */

  getCep(cep) {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`)
  }

  getApiListaEstados(params) {
    return this.http.get(environment.url + params)
  }
}
