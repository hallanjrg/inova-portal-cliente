import { invalid } from '@angular/compiler/src/render3/view/util';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from './api.service';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import { DomainService } from './domain.service';
import validator from 'validar-telefone';
import { Config, Funcionality } from '../models/config';

@Injectable({
  providedIn: 'root'
})
export class AbstractService {

  constructor(private domain: DomainService) { }

  validateForm(form: FormGroup) {
    const { controls } = form;
    for (let c in controls) {
      if (controls[c] && controls[c].invalid) controls[c].markAllAsTouched();
    }
    return form;
  }

  validateFormField(formField: any) {
    if (
      formField && formField.touched && formField.invalid) return true;
  }


  calculateZipCode(zipCode: FormControl) {
    if (zipCode.value.length === 8) return this.domain.getZipCodeResults(zipCode.value);
  }

  getFreight(zipCode, control: string) {
    if (zipCode.value.length === 8 && control === 'endereco') return this.domain.calcFreight(zipCode.value);
  }

  getCitiesByUf(uf: string) {
    if (uf) { return this.domain.getCitiesByUf(uf); }
  }

  cpfCnpjIsReal(cpfCnpj: any, formField: any) {
    if (cpfCnpj.length > 11) {
      if (!cnpj.isValid(cpfCnpj)) {
        formField.setErrors({ 'invalid': true })
      } else {
        formField.setErrors(null)
      }
    } else if (cpfCnpj.length == 11) {
      if (!cpf.isValid(cpfCnpj)) {
        formField.setErrors({ 'invalid': true })
      } else {
        formField.setErrors(null)
      }
    }
  }

  phoneNumberIsReal(telefone: any, formField: any) {
    if (telefone.length >= 10) {
      if (!validator(telefone)) {
        formField.setErrors({ 'invalid': true })
      } else {
        formField.setErrors(null)
      }
    }
  }

  simpleClone(obj: any) {
    return Object.assign({}, obj);
  }

  fullNamePattern() {
    return `[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]*\s.[A-záàâãéèêíïóôõöúçñ\s]*`
  }

  getInnerMessage(inner: any) {
    const html: any = document.getElementsByClassName('innerHTMLMsgClass');
    if (html) {
      for (let item of html) {
        item.childNodes.forEach(p => {
          if (p.style) { p.style.margin = '0' } else return inner;
        });
      }
      return inner.replace(/[&]nbsp[;]/gi, " ");
    } else {
      return inner.replace(/[&]nbsp[;]/gi, " ");
    }
  }

  newSolicitationOptions(config: Config) {
    const certidao = config.funcionalidades.find((funcionality: Funcionality) => funcionality.grupoServico === 'CERTIDAO');
    const escritura = config.funcionalidades.find((funcionality: Funcionality) => funcionality.grupoServico !== 'CERTIDAO' && funcionality.chave !== 'CERTIDAO_REGISTRO_IMOVEIS' );
    return {
      escritura: escritura ? config.funcionalidades.filter((funcionality: Funcionality) => funcionality.grupoServico !== 'CERTIDAO' && funcionality.chave !== 'CERTIDAO_REGISTRO_IMOVEIS') : false,
      certidao: certidao ? config.funcionalidades.filter((funcionality: Funcionality) => funcionality.grupoServico === 'CERTIDAO') : false
    }

  }

}
