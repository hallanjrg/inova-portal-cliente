import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, timeout } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ZipCode } from '../models/zipCode';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  constructor(private http: HttpClient, private login: LoginService) { }

  getZipCodeResults(zipCode: string) {
    return this.http.get(`https://viacep.com.br/ws/${zipCode}/json/`).toPromise();
  }

  getCitiesByUf(uf: string) {
    return this.http.get(`${environment.url}dominio/municipios?uf=${uf}`).toPromise();
  }

  calcFreight(zipCode: string) {
    return this.http.get(`${environment.url}secure/correios/calcularfrete?origem=${this.login.getRegistry.cep}&destino=${zipCode}`)
      .pipe(timeout(10000), catchError(e => { return of(e) })).toPromise()
  }
}
