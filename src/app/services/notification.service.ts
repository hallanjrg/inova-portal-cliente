import { Injectable } from '@angular/core';
import { NotificationItem } from '../models/notification';
import { ApiService } from './api.service';




@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public notifications: Array<NotificationItem> = [];

  get getUnreadNotifications() {
    return this.notifications.filter((notification: NotificationItem) => !notification.lida);
  }

  get getAllNotifiactions() {
    return this.notifications;
  }

  constructor(private api: ApiService) { }

  refreshNotifications() {
    this.api.getMock(`notificacoes`).subscribe((notifications: Array<NotificationItem>) => {
      this.notifications = notifications;
    })
  }

  read(notification: NotificationItem) {
    notification.soundPlayed = true;
    return this.api.putMock(`notificacoes/${notification.id}`, notification).toPromise();
  }



}
