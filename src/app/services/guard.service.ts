import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  constructor(private login: LoginService, private router: Router, private route: ActivatedRoute) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.login.getToken) {
      if (!localStorage.getItem('user')) {
        this.login.setUser(this.login.getToken);
      }
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
