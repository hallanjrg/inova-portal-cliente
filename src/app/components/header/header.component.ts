import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationItem } from 'src/app/models/notification';
import { Registry } from 'src/app/models/registry';
import { LoginService } from 'src/app/services/login.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  /* solução temporária */

  public interval = null;

  /* solução temporária */


  public new: boolean = false;
  public registry = new Registry();
  public cartorio: string;

  get user() {
    return this.login.getUser;
  }

  constructor(private route: ActivatedRoute, private router: Router, private login: LoginService,
    private notificationService: NotificationService) {
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
    this.registry = JSON.parse(localStorage.getItem('registry'));
/*     this.notificationService.refreshNotifications();
 */
  }


  ngOnInit(): void {
  /*   let notifications: Array<NotificationItem> = [];
    this.interval = setInterval(() => {
      this.notificationService.refreshNotifications();
      notifications = this.notificationService.getUnreadNotifications;
      if (notifications.length > 0) {
        notifications.map((notification: NotificationItem) => {
          if (!notification.soundPlayed) {
            !this.new ? this.play(notification) : null;
            this.new = true;
            setTimeout(() => {
              this.new = false;
            }, 4000);
          }
        })
      } else this.new = false;
    }, 5000); */
  }

  async play(notification: NotificationItem) {
    await this.notificationService.read(notification);
    let audio = new Audio();
    audio.src = '../assets/audio/mixkit-positive-notification-951.wav'
    audio.load();
    audio.play();
    this.notificationService.refreshNotifications();
  }

  leave() {
    this.router.navigate(['/login']);
  }

  redirect() {
    this.router.navigate(['/profile'], {
      queryParams: {
        codigoCartorio: this.cartorio
      }
    })
  }


}
