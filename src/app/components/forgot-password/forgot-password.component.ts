import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends AbstractService implements OnInit {

  public success: boolean = false;
  public token: string = '';
  public forgot: FormGroup;
  public toasts = [];
  public errorMesssage: boolean = false;
  public cartorio: string = '';

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal,
    domain: DomainService,
    private route: ActivatedRoute,
    private router: Router,
    private api: ApiService,
    private loginService: LoginService,
    private toastService: ToastService) {
    super(domain)
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
    this.forgot = new FormGroup({
      email: new FormControl('')
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.modal.dismiss();
  }

  validacaoEmail(par) {

  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.forgot.invalid) {
      this.toasts = this.toastService.show('O campo em vermelho é obrigatório', { classname: 'toast-warning' })
    } else {
      this.api.post(`secure/cliente/esqueceusenha`, this.forgot.value, this.token).subscribe(() => {
        this.success = true;
        this.errorMesssage = false;
      }, error => {
        if (error.status === 400) {
          this.router.navigate(['/cadastro']);
          this.success = true;
          this.errorMesssage = true;

        }
      })

    }
  }


}
