import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public cartorio: string = '';
  @Output() close = new EventEmitter();

  get url() {
    return window.location.pathname;
  }

  constructor(private route: ActivatedRoute, private router: Router) {
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
  }

  ngOnInit(): void {
  }

  onClickedOutside(e) {
    if (e.target.id !== 'open-menu') {
      this.close.emit(false);
    }
  }

  redirect(param: string) {
    this.close.emit();
    this.router.navigate([param], {
      queryParams: {
        codigoCartorio: this.cartorio
      }
    })
  }

}
