import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'src/app/models/chat';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { Solicitation } from 'src/app/models/solicitation_list';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent extends AbstractService implements OnInit {

  public chat: FormGroup;
  public screen: number;
  public toasts = [];

  @ViewChild('inputControl') inputControl: any;


  @Input() open: boolean = false;
  @Input() messages: Array<Message> = [];
  @Input() solicitation: Solicitation_Certidao = new Solicitation_Certidao();

  @Output() update = new EventEmitter();

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screen = window.innerWidth;
  }

  get getUser() {
    return this.login.getUser;
  }

  get chatArray(): FormArray {
    return this.chat.get("arquivo") as FormArray;
  }

  public get getRegistry() {
    return this.login.getRegistry;
  }

  public buttonColor: string = '';

  constructor(domain: DomainService, private login: LoginService, private api: ApiService, private route: ActivatedRoute, private toastService: ToastService) {
    super(domain)
    this.screen = window.innerWidth;
    this.chat = new FormGroup({
      arquivo: new FormArray([]),
      codigoSolicitacao: new FormControl(this.route.snapshot.queryParams.solicitation, Validators.required),
      mensagem: new FormControl('', Validators.required)
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.scrollBottom();
  }

  send() {
    this.toastService.remove(this.toasts);
    if (this.chat.valid) {
      this.chat.value.arquivo = this.chat.value.arquivo.map(c => c.codigoArquivo);
      this.api.post(`secure/chat/enviar`, this.chat.value).subscribe(() => {
        this.update.emit();
        this.chat.get('mensagem').setValue('');
        this.clearFormArray();
        this.chat.updateValueAndValidity();
      })
    } else this.toasts = this.toastService.show('Mensagem obrigatória', { classname: 'toast-warning' });
  }

  scrollBottom() {
    let messageBody = document.querySelector('#scrollable')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - (messageBody!.clientHeight - 10)
      }, 1);
    }
  }

  download(message: Message) {
    window.open(message.linkArquivo, '_blank');
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files);
    array.forEach((element: any) => {
      this.toastService.remove(this.toasts);
      if (element.size < 10000000) {
        this.uploadRequest(element);
      } else {
        this.toasts = this.toastService.show(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`, {
          classname: 'toast-warning'
        });
      }
    });
    event.target.value = '';
  }

  uploadRequest(fileList: any) {
    let formData = new FormData();
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.chat.get('arquivo') as FormArray).push(this.addFile(res.codigoArquivo, fileList.name));
    }, () => { });
  }

  addFile(codigoArquivo: string, name: string) {
    return new FormGroup({
      codigoArquivo: new FormControl(codigoArquivo),
      name: new FormControl(name)
    })
  }

  upload() {
    this.inputControl.nativeElement.click();
  }

  removeFile(index: number) {
    this.chatArray.removeAt(index);
    this.chat.updateValueAndValidity();
  }

  clearFormArray() {
    const c: FormArray = this.chatArray;
    c.controls.map((i: FormControl, index: number) => { c.removeAt(index) });
  }

}
