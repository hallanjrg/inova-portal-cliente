import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Config, Funcionality } from 'src/app/models/config';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public config: Config = null;
  public buttonColor: string = '';
  public certidao = null;
  public escritura = null;

  constructor(private modal: NgbActiveModal, private loginService: LoginService, domain: DomainService) {
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void { }

  action(param: string) {
    this.modal.close(param);
  }

  close() {
    this.modal.close();
  }

}
