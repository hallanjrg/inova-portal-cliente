import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  public open: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  menu(e) {
    this.open = e;
  }

}
