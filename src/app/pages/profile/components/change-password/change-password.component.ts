import { ApiService } from 'src/app/services/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/services/toast/toast-service';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent extends AbstractService implements OnInit {

  public changePassword: FormGroup;
  public toasts = [];
  public errorMessage: string = '';

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal,
    private toastService: ToastService,
    private loginService: LoginService,
    domain: DomainService,
    private api: ApiService) {
    super(domain);
    this.changePassword = new FormGroup({
      password: new FormControl('', Validators.required),
      novaSenha: new FormControl('', Validators.required),
      confirmacaoSenha: new FormControl('', Validators.required),
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {

  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.changePassword.invalid) {
      this.changePassword = this.validateForm(this.changePassword);
      this.toasts = this.toastService.show('Os campos em vermelho são obrigatórios', { classname: 'toast-warning' });
    } else {
      this.api.put(`secure/cliente/trocarsenha`, this.changePassword.value).subscribe(() => {
        this.close();
      }, err => {
        if (err.status === 400 && (err.error.msg == 'cadastro.senhaantigainvalida')) {
          this.errorMessage = 'A senha antiga não é válida';
        } else if (err.status === 400 && (err.error.msg == 'cadastro.novasenhadiferente')) {
          this.errorMessage = 'Os campos Senha e Nova Senha devem ser iguais';
        }
      })
    }
  }


  close() {
    this.modal.close();
  }

}
