import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public cadastro: FormGroup;
  public toasts = [];
  public cartorio: string;

  public get getRegistry() {
    return this.login.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private route: ActivatedRoute, private login: LoginService,
    public location: Location,
    private router: Router,
    private modalService: NgbModal,
    private api: ApiService, private toastService: ToastService) {
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
    this.route.data.subscribe(({ data }) => { });

    this.cadastro = new FormGroup({
      nome: new FormControl(this.login.getUser.sub, Validators.required),
      cpfCnpj: new FormControl(''),
      email: new FormControl(this.login.getUser.email),
      telefone: new FormControl(''),
      endereco: new FormGroup({
        cep: new FormControl(''),
        logradouro: new FormControl(''),
        bairro: new FormControl(''),
        complemento: new FormControl(''),
        numero: new FormControl(''),
        estado: new FormControl(''),
        cidade: new FormControl('')
      })
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.cadastro.invalid) {
      this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
    } else {
      this.api.put(`secure/cliente/atualizar`, { nome: this.cadastro.value.nome }).subscribe(() => {
        this.login.updateUserName(this.cadastro.value.nome);
      })
    }
  }

  redirect() {
    this.router.navigate(['/login'], {
      queryParams: {
        codigoCartorio: this.cartorio
      }
    })
  }

  optionChangePassword() {
    this.modalService.open(ChangePasswordComponent, { ariaLabelledBy: 'modal-basic-title', centered: true }).result.then((result) => {

    });
  }

}



