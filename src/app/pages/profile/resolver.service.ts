import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Observable, forkJoin, of, catchError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  private get list() {
    const list = [
      `secure/cliente/minhassolicitacoes`,
      `secure/cliente/minhaspendencias`
    ];
    return list;
  }

  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot) {
    const mappedCalls = this.mapCalls(this.list);
    return forkJoin(mappedCalls);

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      return this.api.get(param).pipe(catchError(() => {
        return of(Error);
      }));
    });
  }
}
