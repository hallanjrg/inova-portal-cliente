import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import { ApiService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Title } from '@angular/platform-browser';
import { AngularFaviconService } from 'angular-favicon';
import { ForgotPasswordComponent } from 'src/app/components/forgot-password/forgot-password.component';
import { ToastService } from 'src/app/services/toast/toast-service';
import { catchError, tap } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends AbstractService implements OnInit {

  public defaultImage: string = '../../../assets/businessman-reading-contract-closeup.jpg';
  public login: FormGroup;
  public toasts = [];
  public cartorio: string = '';
  public success: boolean = false;
  public errorMessage: string;

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  error: any;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router, private api: ApiService,
    public title: Title,
    private loginService: LoginService, private favIcon: AngularFaviconService,
    domain: DomainService,
    private toastService: ToastService) {
    super(domain);
    this.route.data.subscribe(({ data }) => {
      this.loginService.setRegistry(data[0]);
      this.title.setTitle(this.loginService.getRegistry.nome);
      this.favIcon.setFavicon(this.loginService.getRegistry.urlLogo);
      this.buttonColor = this.getRegistry.corCartorio;
      if (data[1] !== Error) {
        this.toasts = this.toastService.show('Conta confirmada com sucessso!', { classname: 'toast-success' });
      }
    })
    this.cartorio = this.loginService.getRegistry.codigo;
    this.login = this.fb.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalService.dismissAll()
  }

  submit() {
    this.errorMessage = null;
    this.toastService.remove(this.toasts);
    if (this.login.invalid) {
      this.login = this.validateForm(this.login);
      this.toasts = this.toastService.show('Os campos em vermelho são obrigatórios', { classname: 'toast-warning' });
    } else {
      this.api.sysToken().subscribe((sys: any) => {
        this.api.post('secure/cliente/login', this.login.value, sys.token).subscribe((retorno: any) => {
          localStorage.setItem('tkn_prt', retorno.token);
          this.loginService.setUser(retorno.token);
          this.router.navigate(['/home'], {
            queryParams: {
              codigoCartorio: this.cartorio
            }
          });
        }, (err: any) => {
          if (err.status === 400 && (err.error.msg == 'login.usuarioousenhainvalido')) {
            this.errorMessage = 'Usuário e senha inválidos';
          } else if (err.status === 400 && (err.error.msg == 'cliente.usuarionaoencontrado')) {
            this.errorMessage = 'Usuário não encontrado';
          } else if (err.status === 400 && (err.error.msg == 'login.confirmarconta')) {
            this.errorMessage = 'Conta não confirmada. Favor acessar o seu e-mail e confirmar a sua conta.'
          }
        })
      })
    }

  }

  redirect() {
    this.router.navigate(['/cadastro']);
  }

  open() {
    const modalRef = this.modalService.open(ForgotPasswordComponent, { windowClass: 'modal-class-custom', size: 'lg' });
    modalRef.componentInstance.token = localStorage.getItem('sys_tkn');
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  validacaoEmail(data: any) {
    if (data) {
      let usuario = data.substring(0, data.indexOf('@'));
      let dominio = data.substring(data.indexOf('@') + 1, data.length);
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search('@') == -1 &&
        dominio.search('@') == -1 &&
        usuario.search(' ') == -1 &&
        dominio.search(' ') == -1 &&
        dominio.search('.') != -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) { }
    }
  }

  autenticar() {
    localStorage.setItem('token', 'autenticado')
    this.router.navigate(['/'])
  }

  cpfCnpjEmailIsReal(data: any) {
    if (data) {
      let usuario = data.substring(0, data.indexOf('@'));
      let dominio = data.substring(data.indexOf('@') + 1, data.length);
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search('@') == -1 &&
        dominio.search('@') == -1 &&
        usuario.search(' ') == -1 &&
        dominio.search(' ') == -1 &&
        dominio.search('.') != -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) { }
      if (data.length > 11) {
        if (!cnpj.isValid(data)) {
          this.error.cpfCnpj.message = 'O CNPJ digitado não é válido.'
        } else {
          this.error.cpfCnpj.message = ''
        }
      } if (data.length == 11) {
        if (!cpf.isValid(data)) {
          this.error.cpfCnpj.message = 'O CPF digitado não é válido.'
          this.login.controls.cpfCnpj.setErrors({ 'invalid': true })
          this.login.controls.cpfCnpj.markAsTouched()
        }
      }
    }
  }
}
