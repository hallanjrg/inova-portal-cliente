import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { ResolverService } from './resolver.service';
import { LazyLoadImageModule } from 'ng-lazyload-image';



@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: LoginComponent,
        resolve: {
          data: ResolverService
        }
      }
    ])
  ]
})
export class LoginModule { }
