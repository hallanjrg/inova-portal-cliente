import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { forkJoin, mergeMap, of, catchError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public sysToken: string;
  public activationToken: string;

  private get list() {
    const list = [
      {
        url: `secure/cartorio/get`,
        method: 'get',
        obj: null
      },
      {
        url: `secure/cliente/confirmarconta?t=${this.activationToken ? this.activationToken : null}`,
        method: 'post',
        obj: null,
        sys: this.sysToken
      },
    ]

    return list;
  }

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  resolve(route: ActivatedRouteSnapshot) {
    localStorage.clear();
    this.activationToken = route.queryParams.t;
    return this.api.sysToken().pipe(mergeMap((ret: any) => {
      this.sysToken = ret.token;
      const mappedCalls = this.mapCalls(this.list);
      localStorage.setItem('sys_tkn', ret.token);
      return forkJoin(mappedCalls);
    }));

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      if (param.method === 'post' && !this.activationToken) { return of(Error) };
      return this.api[param.method](param.url, param.obj, param.sys).pipe(
        catchError(() => {
          return of(Error);
        }));
    });
  }
}
