import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../components/header/header.component';
import { MenuComponent } from '../components/menu/menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';



@NgModule({
  declarations: [
    PagesComponent,
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    ClickOutsideModule,
    NgbModule,
    RouterModule.forChild([
      {
        path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },

      {
        path: 'my-solicitations', loadChildren: () => import('./my-solicitations/my-solicitations.module').then(m => m.MySolicitationsModule)
      },
      {
        path: 'profile', loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
      }
    ])
  ]
})
export class PagesModule { }
