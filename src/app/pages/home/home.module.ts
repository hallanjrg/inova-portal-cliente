import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home.component';
import { ResolverService } from './resolver.service';
import { NotificationsComponent } from './notifications/notifications.component';


@NgModule({
  declarations: [
    HomeComponent,
    NotificationsComponent
  ],
  imports: [
    CommonModule,
    NgbAccordionModule,
    RouterModule.forChild([
      {
        path: '', component: HomeComponent, resolve: {
          data: ResolverService
        }
      }
    ])
  ]
})
export class HomeModule { }
