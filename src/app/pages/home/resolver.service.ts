import { Injectable } from '@angular/core';
import { catchError, forkJoin, of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public cartorio: string;

  private get list() {
    const list = [
      `secure/cliente/minhassolicitacoes`,
      `secure/cliente/listar-notificacoes`,
      `secure/cartorio/obter-configuracoes?codigoCartorio=${this.cartorio}`
    ];
    return list;
  }

  constructor(private api: ApiService) { }

  resolve(route) {
    this.cartorio = route.queryParams.codigoCartorio;
    const mappedCalls = this.mapCalls(this.list);
    return forkJoin(mappedCalls);
  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      return this.api.get(param).pipe(
        catchError(error => {
          return of(error);
        }));
    });
  }
}
