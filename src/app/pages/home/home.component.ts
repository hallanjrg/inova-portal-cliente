import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { Config } from 'src/app/models/config';
import { Solicitation } from 'src/app/models/solicitation_list';
import { solicitation_status, solicitation_status_certidao, solicitation_type } from 'src/app/models/translations';
import { User } from 'src/app/models/user';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ResolverService } from './resolver.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends AbstractService implements OnInit {

  public autoMargin = null;
  public user: User = new User();
  public pendingItems = [];
  public solicitations = [];
  public config: Config = null;
  public notificationsItems = [];
  public finishedSolicitations = [];
  public translate = solicitation_type;
  public chat = [];
  public cartorio: string = '';
  public recentMessages = [];
  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private route: ActivatedRoute, private router: Router, private modalService: NgbModal, private loginService: LoginService, public resolver: ResolverService, domain: DomainService) {
    super(domain);
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.route.data.subscribe(({ data }) => {
      this.buttonColor = this.getRegistry.corCartorio;
      this.recentMessages = data[0].list.filter((sol: Solicitation) => sol.qtdMsgsNaoLidas > 0);
      this.solicitations = data[0].list.filter((sol: Solicitation) => {
        if (sol.tipoServicoEnum.includes('CERTIDAO')) {
          if (sol.codigoStatus !== 4 && sol.codigoStatus !== 5) {
            return sol;
          }
        } else {
          if (sol.codigoStatus !== 9 && sol.codigoStatus !== 10) {
            return sol;
          }
        }
      })
      this.finishedSolicitations = data[0].list.filter((sol: Solicitation) => {
        if (sol.tipoServicoEnum.includes('CERTIDAO')) {
          if (sol.codigoStatus == 4 || sol.codigoStatus == 5) {
            return sol;
          }
        } else {
          if (sol.codigoStatus == 9 || sol.codigoStatus == 10) {
            return sol;
          }
        }
      })
      this.notificationsItems = data[1];
      this.config = data[2];
    })
  }

  ngOnInit(): void {
  }

  detect(event) {
    if (event) { this.autoMargin = true }
    else { this.autoMargin = false }
  }

  redirect(param: string, solicitation: Solicitation) {
    this.router.navigate([param], {
      queryParams: {
        codigoCartorio: this.cartorio,
        code: solicitation.codigoSimples,
        solicitation: solicitation.codigoInterno,
        type: solicitation.tipoServicoEnum.includes('CERTIDAO_REGISTRO') ? 'ESCRITURA' : solicitation.tipoServicoEnum.includes('CERTIDAO') ? 'CERTIDAO' : 'ESCRITURA'
      }
    })
  }

  getStatus(solicitation: Solicitation) {
    if(solicitation.tipoServicoEnum.includes('CERTIDAO_REGISTRO')){
      return solicitation_status[solicitation.codigoStatus].description;
    }else if (solicitation.tipoServicoEnum.includes('CERTIDAO')) {
      return solicitation_status_certidao[solicitation.codigoStatus].description;
    } else {
      return solicitation_status[solicitation.codigoStatus].description;
    }
  }

  new(param: string) {
    this.modalService.dismissAll()
    this.router.navigate(['/my-solicitations/new'], {
      queryParams: {
        codigoCartorio: this.cartorio,
        selectedType: param
      }
    })
  }

  resolve() {
    const { codigoCartorio } = this.route.snapshot.queryParams;
    this.resolver.resolve({ queryParams: { codigoCartorio } }).subscribe((data: any) => {
      this.recentMessages = data[0].list.filter((sol: Solicitation) => sol.qtdMsgsNaoLidas > 0);
      this.solicitations = data[0].list.filter((sol: Solicitation) => {
        if (sol.tipoServicoEnum.includes('CERTIDAO')) {
          if (sol.codigoStatus !== 4 && sol.codigoStatus !== 5) {
            return sol;
          }
        } else {
          if (sol.codigoStatus !== 9 && sol.codigoStatus !== 10) {
            return sol;
          }
        }
      })
      this.finishedSolicitations = data[0].list.filter((sol: Solicitation) => {
        if (sol.tipoServicoEnum.includes('CERTIDAO')) {
          if (sol.codigoStatus == 4 || sol.codigoStatus == 5) {
            return sol;
          }
        } else {
          if (sol.codigoStatus == 9 || sol.codigoStatus == 10) {
            return sol;
          }
        }
      })

      this.notificationsItems = data[1];
      this.config = data[2];
    })
  }

  open() {
    const certidao = this.newSolicitationOptions(this.config).certidao;
    const escritura = this.newSolicitationOptions(this.config).escritura;
    if (escritura && certidao) {
      const modalRef = this.modalService.open(ModalComponent, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
      modalRef.componentInstance.config = this.config
      modalRef.result.then((result) => {
        if (result) {
          this.new(result);
        }
      });
    } else {
      certidao ? this.new('CERTIDOES') : this.new('ESCRITURAS');
    }

  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
