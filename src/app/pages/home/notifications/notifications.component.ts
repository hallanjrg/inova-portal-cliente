import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsItems } from 'src/app/models/notifications';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  public active = false;
  @Input() notificationsItems: Array<NotificationsItems> = [];
  @Output() update = new EventEmitter();
  @Output() action = new EventEmitter();

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit(): void {
  }

  redirect(param: string, notification: NotificationsItems) {
    this.notificationRead(notification);
    this.router.navigate([param], {
      queryParams: {
        codigoCartorio: notification.codigoCartorio,
        code: notification.codigoSimples,
        solicitation: notification.codigoSolicitacao,
        type: notification.tipoSolicitacao.includes('CERTIDAO') ? 'CERTIDAO' : 'ESCRITURA'
      }
    })
  }


  notificationRead(notification: NotificationsItems) {
    this.api.put('secure/notificacao/update', { codigo: notification.codigo, lida: true }).subscribe((res: any) => {
      this.update.emit();
    });
  }

  acionar() {
    this.active = !this.active;
    console.log(this.active);
    if (this.active) { this.action.emit(true) }
    else this.action.emit(false);
  }

}
