import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewComponent } from './new.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResolverService } from './resolver.service';
import { EscriturasComponent } from './components/escrituras/escrituras.component';
import { CertidaoEscrituraComponent } from './components/certidao-escritura/certidao-escritura.component';
import { CertidaoCasamentoComponent } from './components/certidao-casamento/certidao-casamento.component';
import { CertidaoNascimentoComponent } from './components/certidao-nascimento/certidao-nascimento.component';
import { CertidaoObitoComponent } from './components/certidao-obito/certidao-obito.component';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';
import { SuccessComponent } from './components/success/success.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { CertidaoRegistroImoveisComponent } from './components/certidao-registro-imoveis/certidao-registro-imoveis.component';


@NgModule({
  declarations: [
    NewComponent,
    EscriturasComponent,
    CertidaoEscrituraComponent,
    CertidaoCasamentoComponent,
    CertidaoNascimentoComponent,
    CertidaoObitoComponent,
    SuccessComponent,
    ConfirmComponent,
    CertidaoRegistroImoveisComponent
  ],
  imports: [
    CommonModule,
    CurrencyMaskModule,
    ReactiveFormsModule,
    FormsModule,
    NgbNavModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild([
      { path: '', component: NewComponent, resolve: {
        data: ResolverService
      } }
    ])
  ]
})
export class NewModule { }
