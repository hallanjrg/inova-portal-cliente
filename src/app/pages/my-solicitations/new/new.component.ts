import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Config } from 'src/app/models/config';
import { Responsible } from 'src/app/models/responsible';
import { general_type } from 'src/app/models/translations';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent extends AbstractService implements OnInit {

  public form: FormGroup;
  public config: Config = null;
  public types: any;
  public typesCertidao: any;
  public responsible: Array<Responsible> = null;
  public selectedType: string;
  public translation = general_type;
  public states = [];

  constructor(private route: ActivatedRoute, private api: ApiService, domain: DomainService) {
    super(domain);
    this.selectedType = this.route.snapshot.queryParams.selectedType;
    this.route.data.subscribe(({ data }) => {
      this.config = data[0];
      this.types = this.newSolicitationOptions(this.config).escritura;
      this.typesCertidao = this.newSolicitationOptions(this.config).certidao;
      this.typesCertidao.push(
        { chave: 'CERTIDAO_REGISTRO_IMOVEIS', 
        descricao: 'Certidão de Registro de Imóveis', 
        grupoServico: 'CERTIDAO', 
        descricaoGrupoServico: 'Certidão' }
      )
      this.states = Object.entries(data[1]).map(item => item[0]).sort();
      this.responsible = data[2];
    })
  }

  ngOnInit(): void {
  }

}
