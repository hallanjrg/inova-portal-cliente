import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { of, catchError, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public cartorio: string;

  private get list() {
    const list = [
      `secure/cartorio/obter-configuracoes?codigoCartorio=${this.cartorio}`,
      `dominio/estados`,
      `secure/usuario/listar-recebedosite`
    ];
    return list;
  }

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  resolve(route: ActivatedRouteSnapshot) {
    this.cartorio = route.queryParams.codigoCartorio;
    const mappedCalls = this.mapCalls(this.list);
    return forkJoin(mappedCalls);

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      return this.api.get(param).pipe(
        catchError(error => {
          return of(error);
        }));
    });
  }
}
