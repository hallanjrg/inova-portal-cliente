import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { Solicitation_Escritura } from 'src/app/models/solicitation_escritura';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  public solicitationNumber: any;
  public date: Date;

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private router: Router, private modal: NgbActiveModal, private loginService: LoginService,) {
    this.date = new Date();
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
  }

  redirect() {
    this.modal.close();


    /* this.router.navigate([''], {
      queryParams: {
        code: this.solicitation.codigoSimples,
        solicitation: this.solicitation.codigo
      }
    }) */
  }

}
