import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  public solicitation: any;
  public checkedFreight: boolean;
  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal, private loginService: LoginService) {
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.checkFreight()
  }

  close() {
    this.modal.close();
  }

  checkFreight(){
    if(this.solicitation){
      this.solicitation.tipoRetirada == 1 && !this.solicitation.valorFrete ? this.checkedFreight = true : this.checkedFreight = false
    }
  }

  submit() {
    this.modal.close(true);
  }

}
