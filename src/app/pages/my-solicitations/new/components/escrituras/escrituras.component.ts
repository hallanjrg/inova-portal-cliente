import { Responsible } from './../../../../../models/responsible';
import { Location } from '@angular/common';
import { Component, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Config } from 'src/app/models/config';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { Solicitation_Escritura } from 'src/app/models/solicitation_escritura';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { ConfirmComponent } from '../confirm/confirm.component';
import { SuccessComponent } from '../success/success.component';

@Component({
  selector: 'app-escrituras',
  templateUrl: './escrituras.component.html',
  styleUrls: ['./escrituras.component.scss']
})
export class EscriturasComponent extends AbstractService implements OnInit {

  public escritura: FormGroup;
  public toasts = [];
  public optionResponsible: boolean = false;

  @Input() types = [];
  @Input() config: Config;
  @Input() responsible: Array<Responsible>;

  get arquivosArray(): FormArray {
    return this.escritura.get('arquivo') as FormArray;
  }

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private api: ApiService, private router: Router,
    private login: LoginService, public location: Location,
    private modal: NgbModal,
    private toastService: ToastService,
    private loginService: LoginService,
    domain: DomainService,
    ) {
    super(domain);
    this.escritura = new FormGroup({
      arquivo: new FormArray([]),
      canal: new FormControl('2', Validators.required),
      codigoResponsavel: new FormControl(''),
      codigoStatus: new FormControl(1, Validators.required),
      emailSolicitante: new FormControl('', Validators.required),
      ignorarValidacao: new FormControl(true),
      mensagem: new FormControl('', Validators.required),
      nomeSolicitante: new FormControl('', Validators.required),
      telefoneSolicitante: new FormControl('', Validators.required),
      tipoEscritura: new FormControl('', Validators.required),
      tipoSolicitacaoPrecatorio: new FormControl(null),
      valorSolicitacao: new FormControl(null),
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.escritura.get('tipoEscritura').setValue(this.types[0].chave);
    this.escritura.get('nomeSolicitante').setValue(this.login.getUser.sub);
    this.escritura.get('emailSolicitante').setValue(this.login.getUser.email);
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.escritura.invalid) {
      this.escritura = this.validateForm(this.escritura);
      this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' })
    } else {
      this.confirm();
    }
  }

  confirm() {
    const modalRef = this.modal.open(ConfirmComponent, { windowClass: 'modal-class-custom', size: 'lg', centered: true })
    modalRef.result.then((r: boolean) => {
      if (r) {
        this.escritura.value.arquivo = this.escritura.value.arquivo.map(c => c.codigoArquivo);
        this.escritura.value.codigoStatus = this.escritura.value.tipoEscritura == 'REGISTRO_TITULO_IMOVEIS' ? 28 : 1
        this.api.post(`secure/solicitacao/save`, this.escritura.value).subscribe(
          (ret: Solicitation_Certidao | Solicitation_Escritura) => {
            this.escritura.get('arquivo').reset();
            this.arquivosArray.controls = []
            this.escritura.updateValueAndValidity();
            const modalRef = this.modal.open(SuccessComponent, { centered: true, windowClass: 'modal-class-custom', size: 'md' });
            modalRef.componentInstance.solicitationNumber = ret.codigo;
            this.router.navigate(['/my-solicitations'], { queryParams: { codigoCartorio: this.config.codigoCartorio } })
          })
      }
    })
  }

  validatetype() {
    if (this.escritura.get('tipoEscritura').value === 'PRECATORIA') {
      this.escritura.get('valorSolicitacao').setValidators(Validators.required);
      this.escritura.get('tipoSolicitacaoPrecatorio').setValidators(Validators.required);
      this.escritura.get('valorSolicitacao').updateValueAndValidity();
      this.escritura.get('tipoSolicitacaoPrecatorio').updateValueAndValidity();
    } else {
      this.escritura.get('valorSolicitacao').clearValidators();
      this.escritura.get('tipoSolicitacaoPrecatorio').clearValidators();
      this.escritura.get('valorSolicitacao').updateValueAndValidity();
      this.escritura.get('tipoSolicitacaoPrecatorio').updateValueAndValidity();
    }
  }

  optionsResponsible(condition: boolean) {
    if (condition != null) {
      this.optionResponsible = condition
    }
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        alert(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`)
      }
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.escritura.get('arquivo') as FormArray).push(this.addFile(res.codigoArquivo, fileList.name));
    }, err => {
      //
    })
  }

  addFile(codigoArquivo: string, name: string) {
    return new FormGroup({
      codigoArquivo: new FormControl(codigoArquivo),
      name: new FormControl(name)
    })
  }

  removeFile(index: number) {
    this.arquivosArray.removeAt(index);
    this.escritura.updateValueAndValidity();
  }

}
