import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Config, TipoSolicitacao, TipoValor } from 'src/app/models/config';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { Solicitation_Escritura } from 'src/app/models/solicitation_escritura';
import { ActData } from 'src/app/models/solicitation_list';
import { Freight, ZipCode } from 'src/app/models/zipCode';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { ConfirmComponent } from '../confirm/confirm.component';
import { SuccessComponent } from '../success/success.component';

@Component({
  selector: 'app-certidao-escritura',
  templateUrl: './certidao-escritura.component.html',
  styleUrls: ['./certidao-escritura.component.scss']
})
export class CertidaoEscrituraComponent extends AbstractService implements OnInit {

  public active: number = 1;
  public certidao: FormGroup;
  public actDatas: Array<ActData> = [];
  public toasts = [];
  public cities: Array<any> = [];
  public solicitationValue: number;

  @Input() selectedType: string;
  @Input() types = [];
  @Input() config: Config;
  @Input() states = [];

  get certidaoForm(): FormArray {
    return this.certidao.get("dados") as FormArray;
  }

  get arquivosArray(): FormArray {
    return this.certidao.get('arquivos') as FormArray;
  }

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private api: ApiService,
    private router: Router,
    private login: LoginService,
    private toastService: ToastService,
    public modal: NgbModal,
    private loginService: LoginService,
    domain: DomainService) {
    super(domain);
    this.certidao = new FormGroup({
      arquivos: new FormArray([]),
      canal: new FormControl('2'),
      certidaoDigital: new FormControl(false),
      dados: new FormArray([
        this.addActData()
      ]),
      dadosSolicitante: new FormGroup({
        nome: new FormControl('', Validators.required),
        cpfCnpj: new FormControl('', Validators.required),
        responsavel: new FormControl(''),
        email: new FormControl('', Validators.required),
        telefone: new FormControl('', Validators.required)
      }),
      endereco: new FormGroup({
        cep: new FormControl('', Validators.required),
        logradouro: new FormControl('', Validators.required),
        numero: new FormControl('', Validators.required),
        complemento: new FormControl(''),
        uf: new FormControl('', Validators.required),
        bairro: new FormControl('', Validators.required),
        cidade: new FormControl('', Validators.required)
      }),
      enderecoSolicitante: new FormGroup({
        cep: new FormControl('', Validators.required),
        logradouro: new FormControl('', Validators.required),
        numero: new FormControl('', Validators.required),
        complemento: new FormControl(''),
        uf: new FormControl('', Validators.required),
        bairro: new FormControl('', Validators.required),
        cidade: new FormControl('', Validators.required)
      }),
      mensagem: new FormControl('', Validators.required),
      tipoRetirada: new FormControl(null, Validators.required),
      tipoSolicitacao: new FormControl('CERTIDAO_ESCRITURA'),
      valorFrete: new FormControl(0),
      valorSolicitacao: new FormControl(0),
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.solicitationValue = this.config.valoresSolicitacoes.
      find((tipoSolicitacao: TipoSolicitacao) => tipoSolicitacao.tipoSolicitacao === this.selectedType).tipoValor.
      find((tipoValor: TipoValor) => tipoValor.chave === 'VALOR_SOLICITACAO').valor;

    this.certidao.get('dadosSolicitante').get('nome').setValue(this.login.getUser.sub);
    this.certidao.get('dadosSolicitante').get('email').setValue(this.login.getUser.email);
    this.certidao.get('valorSolicitacao').setValue(this.solicitationValue);
  }

  addItem() {
    this.certidaoForm.push(this.addActData());
    let value = this.solicitationValue;
    let quantity = (this.certidao.get('dados') as FormArray).length;
    this.certidao.get('valorSolicitacao').setValue(value * quantity);
  }

  removeItem(i: number) {
    this.certidaoForm.removeAt(i);
    let value = this.solicitationValue;
    let quantity = (this.certidao.get('dados') as FormArray).length;
    this.certidao.get('valorSolicitacao').setValue(value * quantity);
  }

  addActData() {
    return new FormGroup({
      nomePartes: new FormControl(''),
      dataAto: new FormControl(''),
      tipoAto: new FormControl(''),
      livro: new FormControl(''),
      folha: new FormControl('')
    })
  }

  certidaoDigital() {
    if(this.certidao.get('certidaoDigital').value){
      this.certidao.get('endereco').disable();
      this.certidao.get('tipoRetirada').setValue(0);
    }else{
      this.certidao.get('endereco').enable()
    }
  }

  collectionType(type: number) {
    const zipCode = this.certidao.get('endereco').get('cep');
    this.certidao.get('tipoRetirada').setValue(type);
    if (type === 0) {
      this.certidao.get('endereco').disable();
      this.certidao.get('valorFrete').setValue(0);
    } else if (zipCode) {
      this.certidao.get('endereco').enable();
      this.calcZip(zipCode, 'endereco')
    }
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.certidao.invalid) {
      this.certidao = this.validateForm(this.certidao);
      this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
    } else {
      this.confirm();
    }
  }

  confirm() {
    const modalRef = this.modal.open(ConfirmComponent, { windowClass: 'modal-class-custom', size: 'lg', centered: true })
    modalRef.componentInstance.solicitation = this.certidao.value
    modalRef.result.then((r: boolean) => {
      if (r) {
        this.certidao.value.arquivos = this.certidao.value.arquivos.map(c => c.codigoArquivo);
        this.api.post(`secure/solicitacaocertidao/save`, this.certidao.value).subscribe((ret: any) => {
          this.certidao.get('arquivos').reset();
          this.arquivosArray.controls = []
          this.certidao.updateValueAndValidity();
          const modalRef = this.modal.open(SuccessComponent, { windowClass: 'modal-class-custom', size: 'md', centered: true });
          modalRef.componentInstance.solicitationNumber = ret;
          this.router.navigate(['/my-solicitations'], { queryParams: { codigoCartorio: this.config.codigoCartorio } })
        })
      }
    })
  }

  next() {
    this.toastService.remove(this.toasts);
    switch (this.active) {
      case 1:
        if (this.certidao.get('dadosSolicitante').valid && this.certidao.get('mensagem').valid) {
          this.active++;
        } else {
          this.certidao.value.dadosSolicitante = this.validateForm(this.certidao.get('dadosSolicitante') as FormGroup);
          this.certidao.get('mensagem').markAsTouched();
          this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
        }
        return
      case 2:
        if (this.certidao.get('enderecoSolicitante').valid) {
          this.active++;
        } else {
          this.certidao.value.enderecoSolicitante = this.validateForm(this.certidao.get('enderecoSolicitante') as FormGroup);
          this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
        }
        return
      case 3:
        this.active++;
        return
      case 4:
          if (
            this.certidao.valid &&
            this.certidao.get('tipoRetirada').value != null && this.certidao.get('certidaoDigital').value != null
          ) {
            this.submit();
          }else if(this.certidao.get('tipoRetirada').value == null && this.certidao.get('certidaoDigital').value != null){
            this.toasts = this.toastService.show(
              'Preencha uma forma de entrega',
              { classname: 'toast-warning' }
            );
          } else {
            this.certidao.value.endereco = this.validateForm(
              this.certidao.get('endereco') as FormGroup
            );
            this.toasts = this.toastService.show(
              'Os campos marcados em vermelho são obrigatórios.',
              { classname: 'toast-warning' }
            );
          }
          return;
        default:
          break;
    }


  }

  previous() {
    if (this.active > 1) {
      this.active--;
    }
  }

  async calcZip(zipCode: any, controlName: string) {
    const freight: Freight = await this.getFreight(zipCode, controlName) as Freight;
    const results: ZipCode = await this.calculateZipCode(zipCode) as ZipCode;
    if (results) {
      this.certidao.get(controlName).get('logradouro').setValue(results.logradouro);
      this.certidao.get(controlName).get('bairro').setValue(results.bairro);
      this.certidao.get(controlName).get('cidade').setValue(results.localidade);
      this.certidao.get(controlName).get('uf').setValue(results.uf);
      this.changeState(results.uf);
      controlName === 'endereco' ? this.certidao.get('valorFrete').setValue(freight.valor) : null;
    }
  }


  async changeState(uf: string) {
    let cities = await this.getCitiesByUf(uf);
    this.cities = Object.entries(cities).map(city => city[1]);
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        alert(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`)
      }
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.certidao.get('arquivos') as FormArray).push(this.addFile(res.codigoArquivo, fileList.name));
    }, err => {
      //
    })
  }

  addFile(codigoArquivo: string, name: string) {
    return new FormGroup({
      codigoArquivo: new FormControl(codigoArquivo),
      name: new FormControl(name)
    })
  }

  removeFile(index: number) {
    this.arquivosArray.removeAt(index);
    this.certidao.updateValueAndValidity();
  }

}
