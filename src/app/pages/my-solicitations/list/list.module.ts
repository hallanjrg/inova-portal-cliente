import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListComponent } from './list.component';
import { ResolverService } from './resolver.service';



@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    RouterModule.forChild([
      {
        path: '', component: ListComponent,
        resolve: {
          data: ResolverService
        }
      }
    ])
  ]
})
export class ListModule { }
