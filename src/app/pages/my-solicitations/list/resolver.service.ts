import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { of, catchError, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public query;
  public cartorio: string;

  private get list() {
    const list = [
      `secure/cliente/minhassolicitacoes${this.query ? '?' + Object.keys(this.query).map(key => key + '=' + this.query[key]).join('&') : ''}`,
      `secure/cartorio/obter-configuracoes?codigoCartorio=${this.cartorio}`
    ];
    return list;
  }

  constructor(private api: ApiService) { }

  resolve(route: any) {
    this.cartorio = route.queryParams.codigoCartorio;
    const { query } = route.queryParams;
    query ? this.query = query : null;
    const mappedCalls = this.mapCalls(this.list);
    return forkJoin(mappedCalls);

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      return this.api.get(param).pipe(
        catchError(error => {
          return of(error);
        }));
    });
  }
}
