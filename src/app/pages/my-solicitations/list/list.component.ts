import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { Config } from 'src/app/models/config';
import { Solicitation } from 'src/app/models/solicitation_list';
import { solicitation_status, solicitation_status_certidao, solicitation_type } from 'src/app/models/translations';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ResolverService } from './resolver.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends AbstractService implements OnInit {

  public solicitations: Array<Solicitation> = [];
  public config: Config = null;
  public type = solicitation_type;
  public status = solicitation_status;
  public filter = 'solicitation';
  public filterStatus = false;
  public cartorio: string = '';
  public queryParams = {
    codigoSimples: '',
    inicio: '',
    fim: ''
  }

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  public hoveredDate: NgbDate | null = null;

  public fromDate: NgbDate;
  public toDate: NgbDate | null = null;

  constructor(private route: ActivatedRoute,
    private calendar: NgbCalendar, public formatter: NgbDateParserFormatter,
    private router: Router,
    domain: DomainService,
    private loginService: LoginService,
    private modalService: NgbModal,
    private resolver: ResolverService) {
    super(domain);
    this.cartorio = this.route.snapshot.queryParams.codigoCartorio;
    this.buttonColor = this.getRegistry.corCartorio;
    this.fromDate = calendar.getNext(calendar.getToday(), 'd', -15);
    this.toDate = calendar.getToday();
    this.queryParams.inicio = moment(this.fromDate).subtract(1, 'M').format('YYYY-MM-DD');
    this.queryParams.fim = moment(this.toDate).subtract(1, 'M').format('YYYY-MM-DD');
    this.route.data.subscribe(({ data }) => {
      this.solicitations = data[0].list.sort((b, a) => {
        return a.dataCriacao > b.dataCriacao ? 1 : a.dataCriacao < b.dataCriacao ? - 1 : 0;
      });
      this.config = data[1];
    })
  }

  ngOnInit(): void {
  }

  redirect(param: string, solicitation: Solicitation) {
    this.router.navigate([param], {
      queryParams: {
        codigoCartorio: this.loginService.getUser.codigoCartorio,
        code: solicitation.codigoSimples,
        solicitation: solicitation.codigoInterno,
        type: solicitation.tipoServicoEnum.includes('CERTIDAO_REGISTRO') ? 'ESCRITURA' : solicitation.tipoServicoEnum.includes('CERTIDAO') ? 'CERTIDAO' : 'ESCRITURA'
      }
    })
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.queryParams.inicio = `${date.year}-${date.month > 9 ? '' : '0'}${date.month}-${date.day > 9 ? '' : '0'}${date.day}`;
    } else if (this.fromDate && !this.toDate && date && (date.after(this.fromDate) || date === this.fromDate)) {
      this.toDate = date;
      this.queryParams.fim = `${date.year}-${date.month > 9 ? '' : '0'}${date.month}-${date.day > 9 ? '' : '0'}${date.day}`;
    } else {
      this.toDate = null;
      this.fromDate = date;
      this.queryParams.inicio = `${date.year}-${date.month > 9 ? '' : '0'}${date.month}-${date.day > 9 ? '' : '0'}${date.day}`;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) &&
      date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) ||
      this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  addRemoveFilter() {
    this.filterStatus = !this.filterStatus;
    !this.filterStatus ? this.resolve() : null;
  }

  resolve() {
    let clone = this.simpleClone(this.queryParams);
    if (this.filterStatus === false) {
      delete clone.inicio;
      delete clone.fim;
      delete clone.codigoSimples;
    } else if (this.filter === 'solicitation') {
      delete clone.inicio;
      delete clone.fim;
    } else if (this.filter === 'period') {
      delete clone.codigoSimples;
    }
    const { codigoCartorio } = this.route.snapshot.queryParams;
    this.resolver.resolve({
      queryParams: {
        query: {
          ...clone
        },
        codigoCartorio

      }
    }).subscribe((data: any) => {
      this.solicitations = data[0].list;
      this.config = data[1];
    })

  }

  consult() {
    this.resolve();
  }

  open() {
    const certidao = this.newSolicitationOptions(this.config).certidao;
    const escritura = this.newSolicitationOptions(this.config).escritura;
    if (escritura && certidao) {
      const modalRef = this.modalService.open(ModalComponent, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
      modalRef.componentInstance.config = this.config
      modalRef.result.then((result) => {
        if (result) {
          this.new(result);
        }
      });
    } else {
      certidao ? this.new('CERTIDOES') : this.new('ESCRITURAS');
    }
  }

  new(param: string) {
    this.modalService.dismissAll()
    this.router.navigate(['/my-solicitations/new'], {
      queryParams: {
        codigoCartorio: this.cartorio,
        selectedType: param
      }
    })
  }

  getStatus(solicitation: Solicitation) {
    if(solicitation.tipoServicoEnum.includes('CERTIDAO_REGISTRO')){
      return solicitation_status[solicitation.codigoStatus].description;
    }else if (solicitation.tipoServicoEnum.includes('CERTIDAO')) {
      return solicitation_status_certidao[solicitation.codigoStatus].description;
    } else {
      return solicitation_status[solicitation.codigoStatus].description;
    }
  }

}
