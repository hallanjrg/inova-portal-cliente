import { Location } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message } from 'src/app/models/chat';
import { Doc } from 'src/app/models/document';
import { Participant } from 'src/app/models/participant';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { PendingItem } from 'src/app/models/solicitation_list';
import { CERTIDAO_STATUS, ESCRITURA } from 'src/app/models/status';
import { solicitation_status, solicitation_status_certidao, solicitation_subjects } from 'src/app/models/translations';
import { User } from 'src/app/models/user';
import { ApiService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { AcceptDocumentComponent } from './components/accept-document/accept-document.component';
import { ConflictComponent } from './components/conflict/conflict.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { SuccessComponent } from './components/documents/success/success.component';
import { HistoryComponent } from './components/history/history.component';
import { PendingItemComponent } from './components/pending-item/pending-item.component';
import { RejectDocumentComponent } from './components/reject-document/reject-document.component';
import { ResolverService } from './resolver.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  traducao = {
    'EMISSAO_CERTIFICADO': {
      value: 'Emissão de certificado'
    },
    'ESCRITURA': {
      value: 'Escritura'
    },
    'EMISSAO_CERTIFICADO_ESCRITURA': {
      value: 'Escritura + emissão de certificado'
    },
  }

  public screen: number;
  public solicitation: Solicitation_Certidao;
  public participants: Array<Participant> = [];
  public pendingItems: Array<PendingItem> = [];
  public userDocuments: Array<any> = [];
  public user: User = new User();
  public status;
  public subject = solicitation_subjects;
  public messages: Array<Message> = [];
  public registry = JSON.parse(localStorage.getItem('registry'));
  public documents: Array<Doc> = [];
  public motive: string;
  public solicitationType: string = '';
  public innerHTMLMsg = null;
  public statusList = [];
  public phases = ESCRITURA;
  public typeCertificate: Array<any> = [];
  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screen = window.innerWidth;
  }

  constructor(public location: Location,
    private api: ApiService,
    private route: ActivatedRoute,
    private login: LoginService,
    private modal: NgbModal,
    private resolver: ResolverService,
    private loginService: LoginService) {
    this.screen = window.innerWidth;
    this.solicitationType = this.route.snapshot.queryParams.type;
    this.route.data.subscribe(({ data }) => {
      this.solicitation = this.solicitationType === 'CERTIDAO' ? data[0] : Solicitation_Certidao.new(data[0]);
      this.status = this.solicitationType === 'CERTIDAO' ?
        CERTIDAO_STATUS :
        ESCRITURA;
      this.statusList = this.solicitationType === 'CERTIDAO' ?
        this.statusList = [1, 2, 3, 4, 5] :
        this.statusList = [1, 2, 3, 4, 5, 6, 13, 14, 15, 7, 18, 17, 8, 9, 10];
      this.pendingItems = data[1];
      this.messages = data[2];
      this.solicitationType === 'CERTIDAO' ? this.documents = data[4] : this.documents = data[3];
      this.solicitationType === 'CERTIDAO' ? this.status = solicitation_status_certidao : this.status = solicitation_status;
      this.buttonColor = this.getRegistry.corCartorio;
      this.userDocuments = data[5];
      this.participants = data[6];
      this.typeCertificate = data[7];
    })
    this.user = this.login.getUser;
  }

  ngOnInit(): void {

  }

  loadMessages() {
    this.api.get(`secure/chat/listar?codigoSolicitacao=${this.solicitation.codigo}`).subscribe((messages: Array<Message>) => {
      this.messages = messages;
      this.scrollBottom();
    })
  }

  scrollBottom() {
    let messageBody = document.querySelector('#scrollable');
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - (messageBody!.clientHeight - 10);
      }, 1)
    }
  }

  solve(pendingItem: PendingItem) {
    const modalRef = this.modal.open(PendingItemComponent, { centered: true, size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pendingItem = this.pendingItems.find((pi: PendingItem) => pi.codigo === pendingItem.codigo);
    modalRef.result.then(ret => {
      ret ? this.resolve() : null;
    })
  }

  resolve() {
    const { solicitation, code, type } = this.route.snapshot.queryParams;
    this.resolver.resolve({ queryParams: { solicitation, code, type } }).subscribe((data: any) => {
      this.solicitation = this.solicitationType === 'CERTIDAO' ? data[0] : Solicitation_Certidao.new(data[0]);
      this.pendingItems = data[1];
      this.messages = data[2];
      this.solicitationType === 'CERTIDAO' ? this.documents = data[4] : this.documents = data[3];
      this.solicitationType === 'CERTIDAO' ? this.status = solicitation_status_certidao : this.status = solicitation_status;
      this.userDocuments = data[5];
      this.participants = data[6];

    })
  }

  history(pendingItem: PendingItem) {
    const modalRef = this.modal.open(HistoryComponent, { centered: true, size: 'lg' });
    modalRef.componentInstance.messages = pendingItem.mensagens;
  }

  approve(document: Doc) {
    const modalRef = this.modal.open(AcceptDocumentComponent, { centered: true, backdrop: 'static', keyboard: false });
    modalRef.componentInstance.document = document;
    modalRef.componentInstance.solicitation = this.solicitation;
    modalRef.componentInstance.solicitationType = this.solicitationType;
    modalRef.result.then(ret => {
      ret ? this.resolve() : null;
    })
  }

  reject(document: Doc) {
    const modalRef = this.modal.open(RejectDocumentComponent, { centered: true, backdrop: 'static', size: 'lg', keyboard: false });
    modalRef.componentInstance.document = document;
    modalRef.componentInstance.solicitation = this.solicitation;
    modalRef.componentInstance.solicitationType = this.solicitationType;
    modalRef.result.then(ret => {
      ret ? this.resolve() : null;
    })
  }

  uploadDocument(conflict?: FormGroup) {
    const modalRef = this.modal.open(DocumentsComponent, { centered: true, size: 'xl' })
    modalRef.componentInstance.solicitation = this.solicitation;
    modalRef.componentInstance.participants = this.participants;
    conflict ? modalRef.componentInstance.form = conflict : null;
    // caso haja conflito, abre esse modal com as informações preenchidas
    modalRef.result.then(ret => {
      if (ret?.status === 'invalid') {
        const conflict = this.modal.open(ConflictComponent, { centered: true, size: 'lg' })
        conflict.componentInstance.form = ret.form;
        conflict.result.then(conflict => {
          conflict ? this.uploadDocument(conflict) : this.openSuccessComponent();
        });
      } else if (ret === true) this.openSuccessComponent();
      else null
    })
  }

  openSuccessComponent() {
    this.modal.open(SuccessComponent, { centered: true, size: 'md' })
    this.resolve();
  }

  downloadDocument(document: Doc) {
    for (let doc of document.documentos) {
      window.open(doc, '_blank');
    }
  }

  showTooltip(message: string) {
    let seeMore = message.length > 30;
    return seeMore ? 'Ver mensagem' : '';
  }

  getInnerMessage(inner: any, id: string = '', index: number) {
    const html: any = document.getElementById('innerHTMLMsg' + id + index);
    if (html) {
      html.childNodes.forEach((p, index) => {
        if (p.style) {
          p.style.margin = '0';
          p.classList.add('text-truncate');
          p.style.width = `100px`;
          if (index !== 0) { p.style.display = 'none' };
        } else return inner;
      });
      return inner;
    } else {
      return inner.replace(/[&]nbsp[;]/gi, " ");
    }
  }

  getCurrentImageStatus(s: number) {
    let i = this.statusList.indexOf(s);
    let act = this.statusList.indexOf(this.solicitation.codigoStatus);
    return `../../../../assets/phases/${s}${(s == this.solicitation.codigoStatus && this.solicitation.aguardandoCliente ? '_pending' :
      i <= act ? '_completed' : '')}.svg`
  }


  downloadFile(link: string) {
    window.open(link, '_blank');
  }

  translateTypeCertificate(translateTypesCertificate:any){
  return translateTypesCertificate = this.typeCertificate.find(t => t.chave == translateTypesCertificate).descricao
  }

}
