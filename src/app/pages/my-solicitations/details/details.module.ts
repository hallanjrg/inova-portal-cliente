import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbPopoverModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { NgxMaskModule } from 'ngx-mask';
import { ChatModule } from 'src/app/components/chat/chat.module';
import { AcceptDocumentComponent } from './components/accept-document/accept-document.component';
import { ConflictComponent } from './components/conflict/conflict.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { MobileComponent } from './components/documents/mobile/mobile.component';
import { ParticipantsModule } from './components/documents/participants/participants.module';
import { SuccessComponent } from './components/documents/success/success.component';
import { HistoryComponent } from './components/history/history.component';
import { PendingItemComponent } from './components/pending-item/pending-item.component';
import { RejectDocumentComponent } from './components/reject-document/reject-document.component';
import { DetailsComponent } from './details.component';
import { ResolverService } from './resolver.service';

@NgModule({
  declarations: [DetailsComponent,
    PendingItemComponent, HistoryComponent,
    RejectDocumentComponent, AcceptDocumentComponent, DocumentsComponent, ConflictComponent, SuccessComponent, MobileComponent],
  imports: [
    CommonModule,
    ChatModule,
    FormsModule,
    NgbTooltipModule,
    NgbPopoverModule,
    ParticipantsModule,
    CurrencyMaskModule,
    ReactiveFormsModule,
    NgbTooltipModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild([
      {
        path: '', component: DetailsComponent,
        resolve: {
          data: ResolverService
        }
      }
    ])

  ]
})
export class DetailsModule { }
