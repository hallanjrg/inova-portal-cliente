import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { forkJoin, catchError, of } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';

@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public solicitation: string;
  public code: string;
  public type: string;

  private get list() {
    const list = [
      `secure/${this.type === 'CERTIDAO' ? 'solicitacaocertidao' : 'solicitacao'}/obter?codigoSimples=${this.code}&email=${this.login.getUser.email}`,
      `secure/pendencia/listar?codigoSolicitacao=${this.solicitation}`,
      `secure/chat/listar?codigoSolicitacao=${this.solicitation}`,
      `secure/solicitacao/listarregistros?codigo-escritura=${this.code}`,
      `secure/solicitacaocertidao/listarregistros?codigoSolicitacao=${this.solicitation}`,
      `secure/participante/listardocumentos?codigoSolicitacao=${this.solicitation}`,
      `secure/participante/listar-participante?codigoSolicitacao=${this.solicitation}`,
      `secure/solicitacao/listar-tipo-certidao`
    ];
    return list;
  }

  constructor(private api: ApiService, private login: LoginService) { }

  resolve(route) {
    this.solicitation = route.queryParams.solicitation;
    this.code = route.queryParams.code;
    this.type = route.queryParams.type;
    const mappedCalls = this.mapCalls(this.list);
    return forkJoin(mappedCalls);

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      if (param.url) {
      /*   return this.api.getMock(param.url).pipe(
          catchError(error => {
            return of(error);
          })); */
      } else return this.api.get(param).pipe(
        catchError(error => {
          return of(error);
        }));
    });
  }
}
