import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PendingItem } from 'src/app/models/solicitation_list';
import { ApiService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-pending-item',
  templateUrl: './pending-item.component.html',
  styleUrls: ['./pending-item.component.scss']
})
export class PendingItemComponent implements OnInit {

  public form: FormGroup;
  public toasts = [];
  public pendingItem: PendingItem = new PendingItem();
  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal, private api: ApiService, private toastService: ToastService, private loginService: LoginService) {
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      arquivo: new FormArray([]),
      codigoPendencia: new FormControl(this.pendingItem.codigo),
      codigoSolicitacao: new FormControl(this.pendingItem.codigoSolicitacao),
      mensagem: new FormControl('', Validators.required)
    })
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files);
    array.forEach((element: any) => {
      this.toastService.remove(this.toasts);
      if (element.size < 10000000) {
        this.upload(element);
      } else {
        this.toasts = this.toastService.show(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`);
      }
    });
    event.target.value = '';
  }

  upload(fileList: any) {
    let formData = new FormData();
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.form.get('arquivo') as FormArray).push(new FormControl(String(res.codigoArquivo)));
    }, (err) => { }
    );
  }


  close() {
    this.modal.close(false);
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.form.invalid) {
      this.toasts = this.toastService.show('O campo Mensagem é obrigatório');
    } else this.api.post(`secure/chat/enviar`, this.form.value).subscribe(() => {
      this.pendingItem.resolvido = null;
      this.api.post(`secure/pendencia/save`, this.pendingItem).subscribe(() => { this.modal.close(true); })
    });
  }

}
