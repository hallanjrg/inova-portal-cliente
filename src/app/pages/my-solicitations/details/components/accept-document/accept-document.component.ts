import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Doc } from 'src/app/models/document';
import { Solicitation_Certidao } from 'src/app/models/solicitation_certidao';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-accept-document',
  templateUrl: './accept-document.component.html',
  styleUrls: ['./accept-document.component.scss']
})
export class AcceptDocumentComponent extends AbstractService implements OnInit {

  public document: Doc;
  public solicitation: Solicitation_Certidao;
  public form: FormGroup;
  public toasts = [];
  public solicitationType = '';

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(
  domain: DomainService,
  private modal: NgbActiveModal,
  private toastService: ToastService,
  private api: ApiService,
  private loginService: LoginService) {
    super(domain)
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      codigo: new FormControl(this.document.codigo),
      codigoInterno: new FormControl(this.solicitation.codigo),
      valido: new FormControl(true)
    })
  }

  cancel() {
    this.modal.close();
  }

  submit() {
    this.api.post(`secure/${this.solicitationType === 'CERTIDAO' ? 'solicitacaocertidao' : 'solicitacao'}/aprovar-documento`,
      this.form.value).subscribe(() => { this.modal.close(true) });
  }
}
