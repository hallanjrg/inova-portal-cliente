import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Message } from 'src/app/models/chat';
import { Doc } from 'src/app/models/document';
import { AbstractService } from 'src/app/services/abstract.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent extends AbstractService implements OnInit {

  public messages: Array<Message> = [];

  get user() {
    return this.loginService.getUser;
  }

  constructor(domain: DomainService, private modal: NgbActiveModal, private loginService: LoginService) {
    super(domain)
  }

  ngOnInit(): void { }

  close() {
    this.modal.close();
  }


  download(message: Message) {
    window.open(message.linkArquivo, '_blank');
  }
}
