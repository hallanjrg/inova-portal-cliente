import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParticipantsComponent } from './participants.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ParticipantsComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ParticipantsComponent
  ]
})
export class ParticipantsModule { }
