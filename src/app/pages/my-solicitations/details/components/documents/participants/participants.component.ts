import { LoginService } from 'src/app/services/login.service';
import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Participant } from 'src/app/models/participant';
import { Solicitation } from 'src/app/models/solicitation_list';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.scss']
})
export class ParticipantsComponent implements OnInit {


  @Input() solicitation: Solicitation = new Solicitation();
  @Input() file: FormGroup;
  @Input() participants: Array<Participant> = [];

  @Output() emit = new EventEmitter();
  @Output() del = new EventEmitter();
  @Output() assign = new EventEmitter();

  public participant: Participant = new Participant('', '', '');
  public isAdding: boolean = false;

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.buttonColor = this.getRegistry.corCartorio;
   }

  ngOnInit(): void { }


  add() {
    this.participant.codigoSolicitacao = this.solicitation.codigo;
    this.emit.emit(this.participant);
    this.isAdding = false;
    this.participant.nome = '';
  }

  delete(participant: Participant) {
    this.del.emit(participant)
  }

  assignParticipant(participant: Participant) {
    this.assign.emit(
      new FormGroup(
        {
          participant: new FormControl(participant),
          file: new FormControl(this.file.value)
        }
      )
    )

  }

}
