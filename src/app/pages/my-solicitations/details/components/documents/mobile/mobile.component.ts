import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { forkJoin } from 'rxjs';
import { Participant } from 'src/app/models/participant';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-mobile',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss']
})
export class MobileComponent extends AbstractService implements OnInit {

  @Input() public form: FormGroup;
  @Input() public toasts: [];
  public isAdding: number = null;
  @Input() public participants: Array<any> = [];
  @Input() public file: FormGroup = null;
  @Input() public solicitation: any;

  @ViewChild('uploadFile') uploadFile: any;

  get files() {
    return this.form.get('arquivo') as FormArray;
  }

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal,
    private toastService: ToastService,
    private api: ApiService,
    private loginService: LoginService,
    domain: DomainService) {
    super(domain)
    this.form = new FormGroup({
      arquivo: new FormArray([]),
      codigo: new FormControl()
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.form.get('codigo').setValue(this.solicitation.codigo);
  }
  closeModal() {
    this.modal.close()
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files);
    if (array.length + (this.form.get('arquivo') as FormArray).length > 20) {
      this.toasts = this.toastService.show(`O número máximo de arquivos é 20.`);
    } else {
      array.forEach((element: any) => {
        this.toastService.remove(this.toasts);
        if (element.size < 10000000) {
          this.upload(element);
        } else {
          this.toasts = this.toastService.show(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`);
        }
      });
    }
    event.target.value = '';

  }

  multipleUpload() {
    return forkJoin([]);
  }

  upload(fileList: any) {
    let formData = new FormData();
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.form.get('arquivo') as FormArray).push(this.newFile(fileList, res));
    }, (err) => { }
    );
  }

  newFile(file: File, form: any) {
    return new FormGroup({
      name: new FormControl(file.name),
      arquivo: new FormGroup({
        codigo: new FormControl(form.codigoArquivo),
        nome: new FormControl(file.name),
        link: new FormControl(''),
        valido: new FormControl('')
      }),
      participant: new FormControl(null),
      description: new FormControl('', Validators.required)
    })
  }

  addParticipant(file: any, i: number) {
    this.file = file;
    this.isAdding = i;
  }

  activateClick() {
    this.uploadFile.nativeElement.click();
  }

  add(participant: Participant) {
    this.api.post(`secure/participante/cadastrar-participante?codigoSolicitacao=${this.solicitation.codigo}`, participant).subscribe(() => { this.getParticipants() })
  }

  getParticipants() {
    this.api.get(`secure/participante/listar-participante?codigoSolicitacao=${this.solicitation.codigo}`).subscribe((participants: Array<Participant>) => {
      this.participants = participants;
    })
  }

  deleteParticipant(participant: Participant) {
    this.api.delete(`secure/participante/excluir-participante?codigoSolicitacao=${this.solicitation.codigo}&codigoParticipante=${participant.codigo}`).subscribe(() => {
      this.getParticipants()
    })
  }

  assignParticipant(event: any) {
    const i = this.form.get('arquivo').value.findIndex(file => file.arquivo.codigo == event.value.file.arquivo.codigo);
    this.files.controls[i].get('participant').setValue(event.get('participant'));
    this.isAdding = null;
  }

  deleteFile(index: number) {
    this.files.removeAt(index)
  }

  validate() {
    if (this.form.invalid) {
      this.modal.close({ status: 'invalid', form: this.form });
    } else this.sendFiles();
  }

  sendFiles() {
    const list = [];
    this.files.value.forEach(file => {
      list.push({
        body: {
          codigoSolicitacao: this.solicitation.codigo,
          codigoParticipante: file.participant?.value.codigo || '',
          descricao: file.description,
          codigoArquivo: file.arquivo.codigo,
        }
      });
    });
    const mappedCalls = this.mapCalls(list);
    forkJoin(mappedCalls).subscribe(() => { this.modal.close(true) });
  }

  mapCalls(requests: Array<any>) {
    return requests.map(request => {
      return this.api.post('secure/participante/enviardocumento', request.body)
    })
  }



}
