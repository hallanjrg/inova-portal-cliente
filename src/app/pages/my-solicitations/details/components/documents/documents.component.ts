import { LoginService } from 'src/app/services/login.service';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { Participant } from 'src/app/models/participant';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent extends AbstractService implements OnInit {

  public form: FormGroup;
  public toasts: [];
  public isAdding: boolean = false;
  public participants: Array<any> = [];
  public file: FormGroup = null;
  public solicitation: any;
  public screen: number = window.innerWidth;

  @ViewChild('uploadFile') uploadFile: any;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screen = window.innerWidth;
  }

  get files() {
    return this.form.get('arquivo') as FormArray;
  }

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal,
    private toastService: ToastService,
    private loginService: LoginService,
    private api: ApiService,
    domain: DomainService) {
    super(domain)
    this.form = new FormGroup({
      arquivo: new FormArray([]),
      codigo: new FormControl()
    })
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
    this.form.get('codigo').setValue(this.solicitation.codigo);
    this.screen = window.innerWidth;
  }

  closeModal() {
    this.modal.close()
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files);
    if (array.length + (this.form.get('arquivo') as FormArray).length > 20) {
      this.toasts = this.toastService.show(`O número máximo de arquivos é 20.`);
    } else {
      array.forEach((element: any) => {
        this.toastService.remove(this.toasts);
        if (element.size < 10000000) {
          this.upload(element);
        } else {
          this.toasts = this.toastService.show(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`);
        }
      });
    }
    event.target.value = '';

  }

  multipleUpload() {
    return forkJoin([]);
  }

  upload(fileList: any) {
    let formData = new FormData();
    formData.append('arquivo', fileList);
    this.api.post('bucket/upload', formData).subscribe((res: any) => {
      (this.form.get('arquivo') as FormArray).push(this.newFile(fileList, res));
    }, (err) => { }
    );
  }

  newFile(file: File, form: any) {
    return new FormGroup({
      name: new FormControl(file.name),
      arquivo: new FormGroup({
        codigo: new FormControl(form.codigoArquivo),
        nome: new FormControl(file.name),
        link: new FormControl(''),
        valido: new FormControl('')
      }),
      participant: new FormControl(null),
      description: new FormControl('', Validators.required)
    })
  }

  addParticipant(file: any) {
    this.file = file;
    this.isAdding = true;
  }

  activateClick() {
    this.uploadFile.nativeElement.click();
  }

  add(participant: Participant) {
    this.api.post(`secure/participante/cadastrar-participante?codigoSolicitacao=${this.solicitation.codigo}`, participant).subscribe(() => { this.getParticipants() })
  }

  getParticipants() {
    this.api.get(`secure/participante/listar-participante?codigoSolicitacao=${this.solicitation.codigo}`).subscribe((participants: Array<Participant>) => {
      this.participants = participants;
      const { controls } = this.form.get('arquivo') as FormArray;
      for (let participant in controls) {
        if (controls[participant].value.participant &&
          !participants.find(p => p.codigo === controls[participant].value.participant.value.codigo)) {
          controls[participant].get('participant').setValue(null);
        }
      }
    })
  }

  deleteParticipant(participant: Participant) {
    this.api.delete(`secure/participante/excluir-participante?codigoSolicitacao=${this.solicitation.codigo}&codigoParticipante=${participant.codigo}`).subscribe(() => {
      this.getParticipants();
    })
  }

  assignParticipant(event: any) {
    const i = this.form.get('arquivo').value.findIndex(file => file.arquivo.codigo == event.value.file.arquivo.codigo);
    this.files.controls[i].get('participant').setValue(event.get('participant'));
    console.log(event.get('participant'));

    this.isAdding = false;
  }

  deleteFile(index: number) {
    this.files.removeAt(index)
  }

  validate() {
    if (this.form.invalid) {
      this.modal.close({ status: 'invalid', form: this.form });
    } else this.sendFiles();
  }

  sendFiles() {
    const list = [];
    this.files.value.forEach(file => {
      list.push({
        body: {
          codigoSolicitacao: this.solicitation.codigo,
          codigoParticipante: file.participant?.value.codigo || '',
          descricao: file.description,
          codigoArquivo: file.arquivo.codigo,
        }
      });
    });
    const mappedCalls = this.mapCalls(list);
    forkJoin(mappedCalls).subscribe(() => { this.modal.close(true) });
  }

  mapCalls(requests: Array<any>) {
    return requests.map(request => {
      return this.api.post('secure/participante/enviardocumento', request.body)
    })
  }




}
