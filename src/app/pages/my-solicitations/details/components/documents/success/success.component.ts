import { LoginService } from 'src/app/services/login.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modalService: NgbModal , private loginService: LoginService) {
    this.buttonColor = this.getRegistry.corCartorio;
  }

  ngOnInit(): void {
  }

  closeModal() {
    this.modalService.dismissAll()
  }

}
