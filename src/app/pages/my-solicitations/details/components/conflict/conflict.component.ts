import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { forkJoin } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-conflict',
  templateUrl: './conflict.component.html',
  styleUrls: ['./conflict.component.scss']
})
export class ConflictComponent implements OnInit {

  public form: FormGroup;

  public get getRegistry() {
    return this.loginService.getRegistry;
  }

  public buttonColor: string = '';

  constructor(private modal: NgbActiveModal,
    private api: ApiService,
    private loginService: LoginService) {
      this.buttonColor = this.getRegistry.corCartorio;
    }

  ngOnInit(): void {

  }

  closeModal() {
    this.form.markAllAsTouched();
    this.modal.close(this.form);
  }


  sendFiles() {
    const list = [];
    (this.form.get('arquivo') as FormArray).value.forEach(file => {
      list.push({
        body: {
          codigoSolicitacao: this.form.value.codigo,
          codigoParticipante: file.participant?.value.codigo || '',
          descricao: file.description,
          codigoArquivo: file.arquivo.codigo,
        }
      });
    });
    const mappedCalls = this.mapCalls(list);
    forkJoin(mappedCalls).subscribe(() => {
      this.modal.close();
    })
  }

  mapCalls(requests: Array<any>) {
    return requests.map(request => {
      return this.api.post('secure/participante/enviardocumento', request.body)
    })
  }

}
