import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MySolicitationsComponent } from './my-solicitations.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    MySolicitationsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: MySolicitationsComponent, children: [
          { path: '', loadChildren: () => import('./list/list.module').then(m => m.ListModule) },
          { path: 'details', loadChildren: () => import('./details/details.module').then(m => m.DetailsModule) },
          { path: 'new', loadChildren: () => import('./new/new.module').then(m => m.NewModule) }
        ]
      }
    ])
  ]
})
export class MySolicitationsModule { }
