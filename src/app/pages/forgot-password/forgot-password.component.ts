import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AngularFaviconService } from 'angular-favicon';
import { AbstractService } from 'src/app/services/abstract.service';
import { ApiService } from 'src/app/services/api.service';
import { DomainService } from 'src/app/services/domain.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent extends AbstractService implements OnInit {
  
  public defaultImage: string = '../../../assets/businessman-reading-contract-closeup.jpg';
  public toasts = [];
  public forgot: FormGroup;
  public cartorio: string;
  public buttonColor: string;

  public get getRegistry() {
    return this.login.getRegistry;
  }

  closeResult: string;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api: ApiService,
    private route: ActivatedRoute,
    private login: LoginService,
    private modalService: NgbModal,
    private toastService: ToastService,
    private title: Title,
    domain: DomainService,
    private favIcon: AngularFaviconService) {
    super(domain)
    const token = this.route.snapshot.queryParams.t;
    this.route.data.subscribe(({ data }) => {
      this.login.setRegistry(data[0]);
      this.title.setTitle(this.login.getRegistry.nome);
      this.favIcon.setFavicon(this.login.getRegistry.urlLogo);
      this.buttonColor = this.getRegistry.corCartorio;
      if (data[1].status === 401) {
        this.router.navigate(['/login']);
        this.toasts = this.toastService.show('Token expirado. Por gentileza, solicite um novo link de validação novamente.', { classname: 'toast-warning' });
      }
    })
    this.forgot = new FormGroup({
      token: new FormControl(token, Validators.required),
      novaSenha: new FormControl(null, Validators.required),
      confirmacaoSenha: new FormControl(null, Validators.required),
    })
  }

  ngOnInit(): void {
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.forgot.invalid) {
      this.forgot = this.validateForm(this.forgot);
      this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
    } else {
      this.api.sysToken().subscribe((sys: any) => {
        this.login.resetPassword('secure/cliente/redefinirsenha', this.forgot.value, sys.token).subscribe(() => {
          this.router.navigate(['/login']);
          this.toasts = this.toastService.show('Senha alterada com sucesso!', { classname: 'toast-warning' })
        })
      })
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  validacaoEmail(data: any) {
    if (data) {
      let usuario = data.substring(0, data.indexOf('@'));
      let dominio = data.substring(data.indexOf('@') + 1, data.length);
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search('@') == -1 &&
        dominio.search('@') == -1 &&
        usuario.search(' ') == -1 &&
        dominio.search(' ') == -1 &&
        dominio.search('.') != -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) { }
    }
  }

}
