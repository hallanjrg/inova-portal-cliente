import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password.component';
import { RouterModule } from '@angular/router';
import { ResolverService } from './resolver.service';
import { ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';



@NgModule({
  declarations: [
    ForgotPasswordComponent
  ],
  imports: [
    CommonModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: ForgotPasswordComponent, resolve: {
          data: ResolverService
        }
      }
    ])
  ]
})
export class ForgotPasswordModule { }
