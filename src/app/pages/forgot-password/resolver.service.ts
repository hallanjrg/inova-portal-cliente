import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { catchError, forkJoin, of, mergeMap } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ResolverService {

  public sysToken: string;
  public token: string;

  private get list() {
    const list = [
      { url: `secure/cartorio/get`, method: 'get', obj: null },
      { url: `secure/cliente/validartoken`, method: 'post', obj: { token: this.token }, sys: this.sysToken },
    ];
    return list;
  }

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  resolve(route: ActivatedRouteSnapshot) {
    this.token = route.queryParams.t;
    return this.api.sysToken().pipe(mergeMap((ret: any) => {
      this.sysToken = ret.token;
      const mappedCalls = this.mapCalls(this.list);
      localStorage.setItem('sys_tkn', ret.token);
      return forkJoin(mappedCalls);
    }));

  }

  private mapCalls(requests: Array<any>) {
    return requests.map(param => {
      return this.api[param.method](param.url, param.obj, param.sys).pipe(
        catchError(error => {
          return of(error);
        }));
    });
  }
}
