import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SignupComponent } from './signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    CommonModule,
    FormsModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: SignupComponent }
    ])
  ]
})
export class SignupModule { }
