import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import { ApiService } from 'src/app/services/api.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import { Registry } from 'src/app/models/registry';
import { DomainService } from 'src/app/services/domain.service';
import { AbstractService } from 'src/app/services/abstract.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent extends AbstractService implements OnInit {
  
  public defaultImage: string = '../../../assets/businessman-reading-contract-closeup.jpg';
  public cadastro: FormGroup;
  public toasts = [];
  public registry = new Registry();
  public errorMessage: string = '';

  public error = {
    cpfCnpj: {
      message: ''
    },
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }

  public get getRegistry() {
    return this.login.getRegistry;
  }

  public buttonColor: string = '';


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api: ApiService,
    private login: LoginService,
    private toastService: ToastService,
    domain: DomainService) {
    super(domain);
    this.buttonColor = this.getRegistry.corCartorio;
    this.cadastro = this.fb.group({
      nome: [null, Validators.required],
      email: [null, Validators.required],
      password: [null, Validators.required]
    })
    this.registry = this.getRegistry;
  }


  ngOnInit(): void {
  }

  submit() {
    this.toastService.remove(this.toasts);
    if (this.cadastro.invalid) {
      this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' });
    } else {
      this.api.sysToken().subscribe((sys: any) => {
        this.api.post('secure/cliente/cadastrar', this.cadastro.value, sys.token).subscribe(() => {
          this.errorMessage = '';
          this.toasts = this.toastService.
            show('Cadastro criado com sucesso. Verifique o e-mail informado para confirmação do cadastro.',
              { classname: 'toast-warning' })
          this.router.navigate(['/login']);
        }, err => {
          if (err.error.msg === 'cadastro.usuarioexistente') {
            this.errorMessage = 'Usuário já cadastrado';
          }
        })
      })
    }
  }

  validacaoEmail(data: any) {
    if (data) {
      let usuario = data.substring(0, data.indexOf('@'));
      let dominio = data.substring(data.indexOf('@') + 1, data.length);
      if (
        usuario.length >= 1 &&
        dominio.length >= 3 &&
        usuario.search('@') == -1 &&
        dominio.search('@') == -1 &&
        usuario.search(' ') == -1 &&
        dominio.search(' ') == -1 &&
        dominio.search('.') != -1 &&
        dominio.indexOf('.') >= 1 &&
        dominio.lastIndexOf('.') < dominio.length - 1
      ) { }
    }
  }
}
