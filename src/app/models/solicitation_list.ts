export class Solicitation {
    codigoInterno: string;
    codigoSimples: number;
    codigoStatus: number;
    dataCriacao: string;
    nome: string;
    qtdMsgsNaoLidas: number;
    tipoServicoEnum: string;
    email: string;
    mensagem: string;
    telefone: string;
    descricaoServico: string;
    nomeEquipe: string;
    nomeResponsavel: string;
    codigo?: string;
}

export class PendingItem {
    codigo: string;
    codigoSolicitacao: string;
    dataPendencia: string;
    mensagens: Array<any>;
    msg: string;
    notificacoes: number;
    resolvido: boolean;
    sucesso: boolean;
    textoPendencia: string;
    tipoArquivo: string;
}

export class ActData {
    nomePartes: string;
    dataAto: string;
    tipoAto: string;
    termo: string;
    livro: string;
    folha: string;
}