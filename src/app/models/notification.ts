export class NotificationItem {
    tipo: string;
    lida: boolean;
    descricao: string;
    soundPlayed: boolean;
    id: number;
}