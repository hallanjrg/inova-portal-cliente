import { CERTIDAO, Solicitation_Escritura } from "./solicitation_escritura";
import { ActData } from "./solicitation_list";

export class Solicitation_Certidao {
    aguardandoCliente: boolean;
    areaEntrega: number;
    arquivos: Array<string>;
    canal: string;
    cartorioCertidao: string;
    cep: string;
    cepEntrega: string;
    cidade: string;
    cidadeEntrega: string;
    certidaoDigital: boolean;
    certidoes: Array<CERTIDAO>;
    cidadeCertidao: string;
    codigo: string;
    codigoCartorio: string;
    codigoEquipe: string;
    codigoRastreio: string;
    codigoResponsavel: string;
    codigoSimples: number;
    codigoStatus: number;
    complemento: string;
    complementoEntrega: string;
    dados: Array<ActData> = [];
    dataAto: null;
    dataCriacao: Date;
    dataAgendamento: Date;
    dataLimitePausa: Date;
    estado: string;
    estadoCertidao: string;
    estadoEntrega: string;
    frete: number;
    filiacaoCertidao: Filiation;
    folhaAto: null
    identificacaoCertidao: { cpf: null, dataNascimentoCasamento: '1992-06-12T00:00:00.000-03:00', nomeCompleto: null, nomeConjuge1: null, nomeConjuge2: null }
    livroAto: null
    logradouro: string;
    logradouroEntrega: string;
    mensagem: string;
    msg: null
    nomeEquipe: string;
    nomeParte: null
    nomeResponsavel: string;
    notificacoes: Array<Notification>;
    numero: string;
    numeroEntrega: string;
    ordem: null
    origemSolicitacao: string;
    pausado: boolean;
    pessoa: Person = new Person();
    qtdAverbacao: number;
    retirada: number;
    tipoAto: null;
    tipoEntrega: number;
    tipoRetirada: number;
    tipoSolicitacao: string;
    tipoSolicitacaoPrecatorio?: string;
    valorFrete: number;
    valorSolicitacao: number;
    verDepois: boolean;

    static new(solicitation_escritura: Solicitation_Escritura) {
        let n = new Solicitation_Certidao();
        n.areaEntrega = solicitation_escritura.areaEntrega;
        n.cep = solicitation_escritura.cep;
        n.cepEntrega = solicitation_escritura.cepEntrega;
        n.certidoes = solicitation_escritura.certidoes;
        n.cidade = solicitation_escritura.cidade;
        n.cidadeEntrega = solicitation_escritura.cidadeEntrega;
        n.codigo = solicitation_escritura.codigoInterno;
        n.codigoCartorio = solicitation_escritura.codigoCartorio;
        n.codigoEquipe = solicitation_escritura.codigoEquipe;
        n.codigoResponsavel = solicitation_escritura.codigoResponsavel;
        n.codigoSimples = solicitation_escritura.codigo;
        n.codigoStatus = solicitation_escritura.codigoStatus;
        n.complemento = solicitation_escritura.complemento;
        n.complementoEntrega = solicitation_escritura.complementoEntrega;
        n.dataCriacao = solicitation_escritura.dataCriacao;
        n.dataLimitePausa = solicitation_escritura.dataLimitePausa;
        n.dataAgendamento = solicitation_escritura.dataAgendamento;
        n.estado = solicitation_escritura.estado;
        n.estadoEntrega = solicitation_escritura.estadoEntrega;
        n.frete = solicitation_escritura.frete;
        n.logradouro = solicitation_escritura.logradouro;
        n.logradouroEntrega = solicitation_escritura.logradouroEntrega;
        n.mensagem = solicitation_escritura.mensagem;
        n.msg = solicitation_escritura.msg;
        n.nomeEquipe = solicitation_escritura.nomeEquipe;
        n.nomeResponsavel = solicitation_escritura.nomeResponsavel;
        n.notificacoes = solicitation_escritura.notificacoes as any;
        n.numero = solicitation_escritura.numero;
        n.numeroEntrega = solicitation_escritura.numeroEntrega;
        n.pausado = solicitation_escritura.pausado;
        n.pessoa.nome = solicitation_escritura.nomeSolicitante;
        n.pessoa.email = solicitation_escritura.emailSolicitante;
        n.pessoa.telefone = solicitation_escritura.telefoneSolicitante;
        n.retirada = solicitation_escritura.retirada;
        n.verDepois = solicitation_escritura.verDepois;
        n.tipoEntrega = solicitation_escritura.tipoEntrega;
        n.tipoSolicitacao = solicitation_escritura.tipoEscritura;
        n.valorSolicitacao = solicitation_escritura.valorSolicitacao;
        n.tipoSolicitacaoPrecatorio = solicitation_escritura.tipoSolicitacaoPrecatorio;

        return n
    }

}

export class Person {
    codigo: string;
    cpfCnpj: string;
    email: string;
    enderecos: Array<Address>;
    msg: null
    nome: string;
    sucesso: null
    telefone: string;
}

export class Notification {
    codigo: number
    codigoCartorio: string;
    codigoSolicitacao: string;
    dataCriacao: Date;
    descricao: string;
    emailUsuario: string;
    lida: boolean;
    link: string;
    tipoNotificacao: number;
}

export class Address {
    bairro: string;
    cep: string;
    cidade: string;
    complemento: string;
    estado: string;
    logradouro: string;
    numero: string;
    tipoEndereco: string;
}


export class Filiation {
    filiacao1: string;
    filiacao2: string;
}

export class CertificationId {
    cpf: string;
    dataNascimentoCasamento: string;
    nomeCompleto: string;
    nomeConjuge1: string;
    nomeConjuge2: string;
}
