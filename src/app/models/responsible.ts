export class Responsible {
  email: string;
  perfil: string;
  nome: string;
  codigo: string;
  usuarioLogin: string;
  emailComunicacao: string;
  codigoCartorio: string;
  msg: string;
  ativo: boolean;
  permiteVisualizarSolicitacoesSemEquipe: boolean;
  recebeSolicitacoesDoSite: boolean;
  recebeNotificacaoNovaSolicitacao: boolean;
  permiteAtribuirEquipe: boolean;
  cargo: string;
  permiteCartaoDigital: boolean;
  possuiCartaoDigital: boolean;
}
