export class User {
    sub: string;
    codigo: string;
    roles: string;
    iss: string;
    codigoCartorio: string;
    exp: string;
    clientAddress: string;
    email: string;
}