export class Message {
    codigo: number;
    codigoSolicitacao: string;
    dataCriacao: Date;
    linkArquivo: string;
    mensagem: string;
    msgSolicitante: boolean;
    remetente: string;
    sucesso: boolean;
    valido: boolean;
}