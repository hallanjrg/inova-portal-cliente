
export class Config {
    funcionalidades: Array<Funcionality>;
    codigoCartorio: string;
    permiteCertidaoDigital: boolean;
    permitePix: boolean;
    valorAdicional: number;
    valores: number;
    valoresSolicitacoes: Array<TipoSolicitacao>;
}

export class Funcionality {
    chave: string;
    descricao: string;
    descricaoGrupoServico: string;
    grupoServico: string;
}

export class TipoSolicitacao {
    descricaoSolicitacao: string;
    tipoSolicitacao: string;
    tipoValor: Array<TipoValor>;
}

export class TipoValor {
    chave: string;
    descricao: string;
    valor: number;
}