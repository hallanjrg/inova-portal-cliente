export class ZipCode {
    bairro: string;
    cep: string;
    complemento: string;
    ddd: string;
    gia: string;
    ibge: string;
    localidade: string;
    logradouro: string;
    siafi: string;
    uf: string;
}

export class Freight {
    msg: string = '';
    sucesso: boolean = false;
    valor: number = 0;
}