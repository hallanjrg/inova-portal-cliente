export const solicitation_type = {
  CERTIDAO_CASAMENTO: { description: 'Certidão de Casamento' },
  CERTIDAO_ESCRITURA: { description: 'Certidão de Escritura' },
  CERTIDAO_NASCIMENTO: { description: 'Certidão de Nascimento' },
  CERTIDAO_OBITO: { description: 'Certidão de Óbito' },
  ESCRITURA_ATA_NOTARIAL: { description: 'Ata Notarial' },
  ESCRITURA_CESSAO_DIREITOS: { description: 'Escritura de Cessão de Direitos' },
  ESCRITURA_COMPRA_VENDA: {
    description: 'Escritura de Venda e Compra de Bens',
  },
  ESCRITURA_CONFERENCIA_BENS: {
    description: 'Escritura de Conferência de Bens / Integralização de Capital',
  },
  ESCRITURA_DAV_TESTAMENTO_VITAL: {
    description: 'Escritura de Dav (Testamento Vital)',
  },
  ESCRITURA_DECLARACAO: { description: 'Escritura de Declaração' },
  ESCRITURA_DECLARACAO_UNIAO_ESTAVEL: {
    description: 'Escritura de Declaração de União Estável',
  },
  ESCRITURA_DIVISAO_DESDOBRO: {
    description: 'Escritura de Divisão e Desdobro',
  },
  ESCRITURA_DIVORCIO: { description: 'Escritura de Divórcio' },
  ESCRITURA_DOACAO: { description: 'Escritura de Doação de Bens' },
  ESCRITURA_EMANCIPACAO: { description: 'Escritura de Emancipação' },
  ESCRITURA_ENOTARIADO: { description: 'E-Notariado' },
  ESCRITURA_INSTITUICAO_USUFRUTO: {
    description: 'Escritura de Instituição de USUFRUTO',
  },
  ESCRITURA_INVENTARIO_EXTRAJUDICIAL: {
    description: 'Inventário / Sobrepartilha Extrajudicial',
  },
  ESCRITURA_PACTO_ANTENUPCIAL: { description: 'Pacto Antenupcial' },
  ESCRITURA_PROCURACAO_SUBSTABELECIMENTO: {
    description: 'Procuração e Substabelecimento',
  },
  ESCRITURA_RECONHECIMENTO_FILHO: { description: 'Reconhecimento de Filho' },
  ESCRITURA_REVOGACAO_PROCURACAO: { description: 'Revogação de Procuração' },
  ESCRITURA_REVOGACAO_TESTAMENTO: { description: 'Revogação de Testamento' },
  ESCRITURA_TESTAMENTO: { description: 'Testamento' },
  ESCRITURA_USUCAPIAO_EXTRAJUDICIAL: { description: 'Usucapião Extrajudicial' },
  FALE_CONOSCO_COMENTARIOS: { description: 'Comentários' },
  FALE_CONOSCO_DENUNCIA: { description: 'Denúncia' },
  FALE_CONOSCO_ENVIAR_CURRICULO: { description: 'Enviar Currículo' },
  FALE_CONOSCO_LGPD: { description: 'LGPD' },
  FALE_CONOSCO_OUVIDORIA: { description: 'Ouvidoria' },
  FALE_CONOSCO_RECLAMACAO: { description: 'Reclamação' },
  FALE_CONOSCO_SUGESTAO: { description: 'Sugestão' },
  FIRMAS_AUTENTICACAO_ABERTURA_FIRMA: { description: 'Abertura de Firma' },
  FIRMAS_AUTENTICACAO_APOSTILAMENTO: { description: 'Apostilamento' },
  FIRMAS_AUTENTICACAO_AUTENTICACAO_COPIA: {
    description: 'Autenticação de Cópia',
  },
  FIRMAS_AUTENTICACAO_CARTA_SENTENCA: { description: 'Carta de Sentença' },
  FIRMAS_AUTENTICACAO_MATERIALIZACAO_DOCUMENTO: {
    description: 'Materialização de Documento',
  },
  FIRMAS_AUTENTICACAO_MENSALISTA: { description: 'Mensalista' },
  FIRMAS_AUTENTICACAO_RECONHECIMENTO_FIRMA: {
    description: 'Reconhecimento de Firma',
  },
  FIRMAS_AUTENTICACAO_RECONHECIMENTO_SINAL_PUBLICO: {
    description: 'Reconhecimento de Sinal Público',
  },
  REGISTRO_CIVIL_AVERBACAO: { description: 'Averbação' },
  REGISTRO_CIVIL_REGISTRO_CASAMENTO: { description: 'Registro de Casamento' },
  REGISTRO_CIVIL_REGISTRO_NASCIMENTO: { description: 'Registro de Nascimento' },
  REGISTRO_CIVIL_REGISTRO_OBITO: { description: 'Registro de Óbito' },
};

export const solicitation_status = {
  1: {
    code: 1,
    description: 'Caixa de entrada',
  },

  2: {
    code: 2,
    description: 'Em análise',
  },
  3: {
    code: 3,
    description: 'Execução',
  },
  4: {
    code: 4,
    description: 'Aprovação de minuta',
  },
  5: {
    code: 5,
    description: 'Agendamento',
  },
  6: {
    code: 6,
    description: 'Lavrado',
  },
  7: {
    code: 7,
    description: 'Registro de imóveis',
  },
  8: {
    code: 8,
    description: 'Retirada',
  },
  9: {
    code: 9,
    description: 'Concluído',
  },
  10: {
    code: 10,
    description: 'Encerrado',
  },
  11: {
    code: 11,
    description: 'Aguardando cliente',
  },
  13: {
    code: 13,
    description: 'Já conferido - Com pendência',
  },
  14: {
    code: 14,
    description: 'Já conferido - Liberar traslado com pendência',
  },
  15: {
    code: 15,
    description: 'Liberar traslado',
  },
  16: {
    code: 16,
    description: 'Traslado assinado',
  },
  17: {
    code: 17,
    description: 'Prazos e exigências',
  },
  18: {
    code: 18,
    description: 'Prenotação',
  },
  19: {
    code: 19,
    description: 'Contato com o cliente',
  },
  20: {
    code: 20,
    description: 'Certificado emitido',
  },
  21: {
    code: 21,
    description: 'Conferência de minutas',
  },
  22: {
    code: 22,
    description: 'Agendar assinatura',
  },
  23: {
    code: 23,
    description: 'Coleta de assinatura credores',
  },
  24: {
    code: 24,
    description: 'Coleta de assinatura pjus',
  },
  25: {
    code: 25,
    description: 'Aprovação',
  },
  26: {
    code: 26,
    description: 'Conferido',
  },

  28: {
    code: 28,
    description: 'Recepção do título / Atendimento',
  },
  29: {
    code: 29,
    description: 'Indexação',
  },
  30: {
    code: 30,
    description: 'Qualificação do título',
  },
  31: {
    code: 31,
    description: 'Pós conferência do título',
  },
  32: {
    code: 32,
    description: 'Encaminhado registro do título / Averbação',
  },
  33: {
    code: 33,
    description: 'Selado',
  },
  34: {
    code: 34,
    description: 'Assinatura',
  },
  35: {
    code: 35,
    description: 'Emissão de Certidão e Digitalização',
  },
  36: {
    code: 36,
    description: 'Pronto para retirada',
  },
  37: {
    code: 37,
    description: 'Confirmar pagamento'
  }
};

export const solicitation_status_certidao = {
  1: {
    code: 1,
    description: 'Caixa de entrada',
  },

  2: {
    code: 2,
    description: 'Confirmar pagamento',
  },
  3: {
    code: 3,
    description: 'Lavrar ato',
  },
  4: {
    code: 4,
    description: 'Concluído',
  },
  5: {
    code: 5,
    description: 'Encerrado',
  },
  6: {
    code: 6,
    description: 'Aguardando cliente',
  },
};

export const solicitation_subjects = {
  MINUTA_PREVIA: { description: 'Minuta prévia' },
  ORCAMENTO: { description: 'Orçamento' },
  OUTROS: { description: 'Outros' },
};

export const general_type = {
  ESCRITURAS: {
    description: 'Escrituras e outros',
  },
  CERTIDOES: {
    description: 'Certidões',
  },
  CERTIDAO_ESCRITURA: {
    description: 'Certidão de escritura',
  },
  CERTIDAO_NASCIMENTO: {
    description: 'Certidão de nascimento',
  },
  CERTIDAO_CASAMENTO: {
    description: 'Certidão de casamento',
  },
  CERTIDAO_OBITO: {
    description: 'Certidão de óbito',
  },
  CERTIDAO_REGISTRO_IMOVEIS: {
    description: 'Certidão de Registro de Imóveis',
  },
};
