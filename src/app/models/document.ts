export class Doc {
    aprovado: boolean;
    codigo: number;
    dataCriacao: Date;
    descricao: string;
    descricaoTipoAssuntoOutros: string;
    documentos: Array<string>;
    message: string;
    motivo: string;
    statusEnvio: string;
    tipoAssunto: string;
}