export class Participant {
    codigo: string;
    nome: string;
    codigoSolicitacao: string;
    id: number;

    constructor(codigo: string, codigoSolicitacao: string, nome: string) {
        this.codigo = codigo;
        this.nome = nome;
        this.codigoSolicitacao = codigoSolicitacao;
    }
}