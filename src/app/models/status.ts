export const CERTIDAO_STATUS = {
    0: {
        url: 'solicitacaocertidao/listar-kanban',
        filtros: [

            { id: 1, filtro: 'Solicitação', key: 'codigoSimples', value: '', options: [] },
            { id: 2, filtro: 'CPF/CNPJ', key: 'cpfCnpj', value: '', options: [] },
            { id: 3, filtro: 'Nome', key: 'nome', value: '', options: [] },
            { id: 4, filtro: 'Responsável', key: 'codigoResponsavel', value: '', options: [] },
            {
                id: 5, filtro: 'Origem', key: 'origem', value: '', options: [
                    { value: '', description: 'Todas' },
                    { value: 'EXTERNO', description: 'Externo' },
                    { value: 'BALCAO', description: 'Balcão' },
                    { value: 'MENSALISTA', description: 'Mensalista' },
                    { value: 'OFICIO', description: 'Ofício' },
                    { value: 'ESCREVENTE', description: 'Escrevente' },
                ]
            }
        ],
        descricao: 'Certidoes',
        urlDetalhes: 'CERTIDAO'
    },
    1: {
        code: 1,
        value: 'Caixa de entrada'
    },
    2: {
        code: 2,
        value: 'Confirmar pagamento'
    },
    3: {
        code: 3,
        value: 'Lavrar ato'
    },
    4: {
        code: 4,
        value: 'Concluído'
    },
    5: {
        code: 5,
        value: 'Encerrado'
    },
    6: {
        code: 6,
        value: 'Aguardando cliente'
    }

}

export const ESCRITURA = {
    0: {
        url: 'solicitacao/listar-kanban',
        filtros: [
            { id: 1, filtro: 'Solicitação', key: 'codigoSimples', value: '', options: [] },
            { id: 3, filtro: 'Nome', key: 'nome', value: '', options: [] },
            { id: 4, filtro: 'Responsável', key: 'codigoResponsavel', value: '', options: [] }
        ],
        descricao: 'Escrituras',
        urlDetalhes: 'SOLICITACAO'
    },
    1: {
        code: 1,
        value: 'Caixa de entrada'
    },

    2: {
        code: 2,
        value: 'Em análise'
    },
    3: {
        code: 3,
        value: 'Execução'
    },
    4: {
        code: 4,
        value: 'Aprovação de minuta'
    },
    5: {
        code: 5,
        value: 'Agendamento'
    },
    6: {
        code: 6,
        value: 'Lavrado - Para conferência'
    },
    7: {
        code: 7,
        value: 'Registro de imóveis'
    },
    8: {
        code: 8,
        value: 'Retirada'
    },
    9: {
        code: 9,
        value: 'Concluído'
    },
    10: {
        code: 10,
        value: 'Encerrado'
    },
    11: {
        code: 11,
        value: 'Aguardando cliente'
    },
    13: {
        code: 13,
        value: 'Já conferido - Com pendência'
    },
    14: {
        code: 14,
        value: 'Já conferido - Liberar traslado com pendência'
    },
    15: {
        code: 15,
        value: 'Liberar traslado'
    },
    16: {
        code: 16,
        value: 'Traslado assinado'
    },
    17: {
        code: 17,
        value: 'Prazos e exigências'
    },
    18: {
        code: 18,
        value: 'Prenotação'
    },
    19: {
        code: 19,
        value: 'Contato com o cliente'
    },
    20: {
        code: 20,
        value: 'Certificado emitido'
    },
    21: {
        code: 21,
        value: 'Conferência de minutas'
    },
    22: {
        code: 22,
        value: 'Agendar assinatura'
    },
    23: {
        code: 23,
        value: 'Coleta de assinatura credores'
    },
    24: {
        code: 24,
        value: 'Coleta de assinatura pjus'
    },
    25: {
        code: 25,
        value: 'Aprovação'
    },
    26: {
        code: 26,
        value: 'Conferido'
    },


}