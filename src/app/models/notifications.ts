export class NotificationsItems {
  codigo: number;
  codigoCartorio: string;
  descricao: string;
  link: string;
  lida: boolean;
  codigoSimples: string;
  tipoSolicitacao: string;
  emailUsuario: string;
  dataCriacao: Date;
  codigoSolicitacao: string;
  tipoNotificacao: number;
  notificacaoExterna: boolean;
}
