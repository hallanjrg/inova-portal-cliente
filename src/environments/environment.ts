// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlLogin: 'http://localhost:3000',
  url: 'https://hml.inovacartorios.com.br/inova-cartorios/api/v1/'
};
  